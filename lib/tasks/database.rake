namespace :db do
  desc 'rollback migration, than migrate it'
  task :tap do
    down = ENV['VERSION'] ? 'db:migrate:down' : 'db:rollback'
    up = ENV['VERSION'] ? 'db:migrate:up' : 'db:migrate'
    [down, up].each { |x| Rake::Task[x].invoke }
  end
end
