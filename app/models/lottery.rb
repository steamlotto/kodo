module Lottery
  LotteryAlreadyFinished = Class.new(StandardError)
  LotteryNotAvailable = Class.new(StandardError)

  delegate :respond_to_missing?, to: Lottery::Base
  def method_missing(*args)
    Lottery::Base.send(*args)
  end
end
