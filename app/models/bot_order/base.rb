class BotOrder::Base < ActiveRecord::Base
  self.table_name = :bot_orders
  after_initialize :set_defaults

  private

  def set_defaults
    self[:status] ||= 'pending'
  end
end
