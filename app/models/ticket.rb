class Ticket < ActiveRecord::Base
  belongs_to :user

  validates :title, :body, :user_name, presence: true
  validates :user_email, email: true

  enum category: %w[issues features else].map { |x| [x, x] }.to_h

  scope :open, -> { where(closed: false) }
  scope :closed, -> { where(closed: true) }

  def open?
    !closed?
  end

  def open!
    self[:closed] = false
  end

  def close!
    self[:closed] = true
  end
end
