class ItemClassSearch < ApplicationSearch
  attribute :name, String

  def scope
    @scope ||= ItemClass.all
  end

  def filter_by_name!
    scope.where! <<-SQL, name: "%#{name}%"
      (lower(market_name) LIKE lower(:name))
      OR
      (lower(market_hash_name) LIKE lower(:name))
    SQL
  end
end
