class LoginNumber < ActiveRecord::Base

  def self.take_one
    LoginNumber.last
  end

  def self.participate(number)
    if LoginNumber.last.parametr!=number
      LoginNumber.create(parametr: number, updated_at: Time.now)
      true
    else
      false
    end

  end

end