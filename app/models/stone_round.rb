class StoneRound < ActiveRecord::Base

  def self.autobet_except(bet)
    if bet=='stone'
      %w(scissors paper)[rand(0..1)]
    elsif bet=='paper'
      %w(stone scissors)[rand(0..1)]
    elsif bet=='scissors'
      %w(stone paper)[rand(0..1)]
    end
  end

  def self.autobet
    %w(stone scissors paper)[rand(0..2)]
  end

  def self.bet_check(bet)
    case bet
      when 'paper'
        return 'paper'
      when 'scissors'
        return 'scissors'
      when 'stone'
        return 'stone'
      else
        return autobet
    end
  end

  def self.bet(user,bet,game_id) #Сделать ставку
    #Корректируем ход
    #Ходим
    performed_bet=self.bet_check(bet)
    rounds=StoneRound.where(stone_game:game_id)
    if !closed?(rounds.last)
      if user==1
        rounds.last.update(user1_pass:performed_bet)
      elsif user==2
        rounds.last.update(user2_pass:performed_bet)
      end
    end
    if draw?(rounds.last)
      redraw!(rounds.last)
    end
    if rounds.last.user1_pass and rounds.last.user2_pass
      write_round!(rounds.last)
      StoneRound.create(stone_game:game_id,created_at:Time.now+5)
    end
    write_round!(rounds.last)
  end

  def self.draw?(round) #Ничья?
    if round.user1_pass and round.user2_pass and round.user1_pass==round.user2_pass
      true
    else
      false
    end
  end

  def self.redraw!(round) #Переиграть
    round.update(user1_pass: nil, user2_pass: nil, created_at: Time.now+5, status: 'draw')
  end

  def self.get_rounds(game_id)
    StoneRound.where(stone_game: game_id).order(:id)
  end

  def self.closed?(round)
    if round.created_at+35<Time.now
      close!(round)
      return true
    else
      return false
    end
  end

  def self.close!(round)
    if round.user1_pass.nil?
      if round.user2_pass
        round.update(user1_pass:autobet_except(round.user2_pass))
      else
        round.update(user1_pass:autobet)
      end
    end
    if round.user2_pass.nil?
      round.update(user2_pass:autobet_except(round.user1_pass))
    end
    write_round!(round)
  end

  def self.check(game_id)
    rounds=StoneRound.where(stone_game:game_id)
    rounds.each do |round|
      if !closed?(round)
        true
      else
        write_round!(round)
        false
      end
    end
  end



  def self.write_round!(round)
    winner=calc_winner(round)
    if winner==1
      round.update(status: User.find(StoneGame.find(round.stone_game).user1).nickname)
    elsif winner==2
      round.update(status: User.find(StoneGame.find(round.stone_game).user2).nickname)
    elsif winner=="user2_wait"
      round.update(status: 'user2_wait')
    elsif winner=="user1_wait"
      round.update(status: 'user1_wait')
    end
  end



  def self.calc_winner(round)
    if round.user2_pass.nil? and round.user1_pass.nil?
      return 'empty'
    elsif (round.user2_pass.nil? and round.user1_pass) or (round.user1_pass.nil? and round.user2_pass)
      if round.user2_pass.nil?
        return 'user1_wait'
      elsif round.user1_pass.nil?
        return 'user2_wait'
      end
    else
      case round.user1_pass
        when "paper"
          case round.user2_pass
            when "scissors"
              return 2
            when "stone"
              return 1
            when "paper"
              return 0
          end
        when "stone"
          case round.user2_pass
            when "scissors"
              return 1
            when "paper"
              return 2
            when "stone"
              return 0
          end
        when "scissors"
          case round.user2_pass
            when "paper"
              return 1
            when "stone"
              return 2
            when "scissors"
              return 0
          end
        else
          return 'error'
      end
    end
  end

  def self.game_winner(game)
    rounds=StoneRound.get_rounds(game)
    user1_score=0
    user2_score=0
    rounds.each do |round|
      case calc_winner(round)
        when 1
          user1_score+=1
        when 2
          user2_score+=1
      end
    end
    if user1_score>user2_score
      return game.user1
    else
      return game.user2
    end
  end




end
