class Role
  def self.for(role)
    "Roles::#{role.camelize}".constantize
  end

  def initialize(&block)
    instance_eval(&block)
  end

  def access(controller)
    params = controller.params
    can? params[:controller], params[:action]
  end

  attr_accessor :admin_scope_access
  alias_method :admin_scope_access?, :admin_scope_access

  def can?(controller, action = :manage)
    c = access_hash[controller.to_sym]
    c[:manage] || c[action.to_sym]
  end

  protected

  def allow(controller, actions = :manage, **options)
    actions = [actions] unless actions.is_a? Array
    controller = controller.to_sym
    x = access_hash[controller.to_sym]
    actions.each do |action|
      action = action.to_sym
      x[action] ||= {}
      x[action].merge! options
    end
    access_hash[controller] = x
  end

  def inherit(role)
    @access_hash = role.send(:access_hash).deep_dup.deep_merge(access_hash)
  end

  private

  def access_hash
    @access_hash ||= Hash.new { {} }
  end
end
