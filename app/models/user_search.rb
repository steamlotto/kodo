class UserSearch < ApplicationSearch
  substring_filter :nickname, String
  range_filter :registered, Time, as: :created_at
  range_filter :payments, Float
  range_filter :kodo_tickets, Integer
  range_filter :headshot_tickets, Integer

  attribute :registered

  def registered=(daterange)
    from, till = daterange.split('-').map { |x| x.sub(%r{\A\s*(\d+)/(\d+)/}, '\2/\1') }
    self.registered_from = from
    self.registered_till = till
  end

  def registered
    from, till = [registered_from, registered_till]
                 .map { |x| I18n.l(x.to_date, format: :datepicker) rescue return }

    "#{from} - #{till}"
  end

  def scope
    @scope ||= User.all
  end

  private

  def with_creditings
    scope.includes(:creditings)
  end
end
