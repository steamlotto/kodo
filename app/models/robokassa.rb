class Robokassa
  include MountConfig
  include ActionView::Helpers::TagHelper

  self.config_file = Rails.root.join 'config/models/robokassa.yml'

  ROBOKASSA_URL = 'http://auth.robokassa.ru/Merchant/Index.aspx'
  #ROBOKASSA_URL = 'http://test.robokassa.ru/Index.aspx'
  REQUIRED_PARAMS = %i[merchant_login signature_value culture out_sum
                       encoding invoice_id shp_uid]

  NAMING_RULES = {
      '_id' => 'ID',
      'shp_' => 'SHP_',
  }

  attr_accessor :out_sum, :shp_uid
  attr_writer :invoice_id

  # @param [ActionController::Parameters] params raw params from controller
  def self.process_result!(params)
    out_sum = params.require 'OutSum'
    shp_uid = params.require 'SHPUid'
    invoice_id = params.require 'InvId'

    instance = new amount: out_sum, user_id: shp_uid, invoice_id: invoice_id
    crc_value = params.require('SignatureValue').downcase

    instance.validate! crc_value
    instance.create_crediting!
  end

  def initialize(params)
    self.out_sum = params.fetch :amount
    self.shp_uid = params.fetch :user_id
    self.invoice_id = params.fetch :invoice_id if params.key? :invoice_id
  end

  # @raise [SecurityError]
  def validate!(crc_value)
    expectation_base = "#{out_sum}:#{invoice_id}:#{merchant_password_2}:SHPUid=#{shp_uid}"
    expectation = Digest::MD5.hexdigest expectation_base
    return if crc_value == expectation
    raise SecurityError, "Robokassa crc value mismatch! Expected #{crc_value}, got #{expectation}"
  end

  # @raise [SecurityError]
  def create_crediting!
    if Crediting.find_by id: invoice_id
      raise SecurityError, "Robokassa confirmed ID=#{invoice_id} twice!"
    end

    Crediting.create amount: out_sum, id: invoice_id, user_id: shp_uid,
                     source: "Робокасса (INVOICE_ID = #{invoice_id})"
  end

  def to_url
    "#{ROBOKASSA_URL}?#{url_params.map { |k, v| "#{k}=#{v}" }.join('&')}"
  end

  def url_params
    REQUIRED_PARAMS.map do |name|
      key = NAMING_RULES.reduce(name.to_s) { |base, (k, v)| base.gsub(k, v) }.camelize
      [key, public_send(name)]
    end
  end

  def invoice_id
    @invoice_id ||= Crediting.generate_id
  end

  def signature_value
    base = "#{merchant_login}:#{out_sum}:#{invoice_id}:#{merchant_password_1}:SHPUid=#{shp_uid}"
    Digest::MD5.hexdigest(base)
  end

  def culture
    'ru'
  end

  def encoding
    'utf-8'
  end
end
