module ItemClassCache
  extend TableCacher

  scope do
    ItemClass.enabled.joins(:items).where(items: { bot_order: nil })
      .group(ItemClass.attribute_names)
      .select('item_classes.*, count(items.id) as items_count')
  end

  def self.rarenesses
    Rails.cache.fetch(key :rarenesses) { all.map(&:rareness).uniq }
  end

  def self.weapon_types
    Rails.cache.fetch(key :weapon_types) { all.map(&:weapon_type).uniq }
  end

  def self.as_json
    Rails.cache.fetch(key :as_json) do
      {
        items: all.map(&:decorate),
        rarenesses: rarenesses,
        weapon_types: weapon_types,
      }
    end
  end

  def self.before_reload!
    super
    (methods(false) - %i[before_reload!]).each do |x|
      Rails.cache.delete(key x)
    end
  end
end
