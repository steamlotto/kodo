class Crediting < ActiveRecord::Base
  def self.generate_id
    connection.execute(<<-SQL).take(1).first['nextval']
      SELECT nextval('creditings_id_seq');
    SQL
  end

  belongs_to :user

  after_create :process_creation!

  private

  def process_creation!
    user.increase_balance! amount, payment: true
  end
end
