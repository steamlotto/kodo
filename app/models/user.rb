class User < ActiveRecord::Base
  LowBalance = Class.new(StandardError)
  include Synchronizable

  validates :steam_offer_link,
            format: {
              with: %r{https?://steamcommunity.com/tradeoffer/new/\?partner=\d+&token=.+},
              allow_blank: true,
            }

  before_create { self[:role] = 'user' if self[:role] == 'guest' }

  has_many :lottery_participations, class_name: 'Lottery::Participation'
  has_many :bot_orders, class_name: 'BotOrder::SendItems'
  has_many :creditings
  has_many :tickets
  has_many :fifties
  has_many :stones

  def self.getprofile(id)
    temp=User.find(id)
    result={}
    result['nickname']=temp.nickname
    result['avatar']=temp.avatar
    return result
  end

  def items
    Item.joins(:bot_order).where(bot_orders: { user_id: id })
  end

  delegate :roles_available, to: Role

  def role
    Role.for(self[:role])
  end

  def increase_balance!(amount, payment: false)
    synchronize(:increase_balance!) { _increase_balance! amount, payment: payment }
  end

  private

  def _increase_balance!(amount, payment:)
    update_string = "balance = balance + #{amount}"
    update_string << ", payments = payments + #{amount}" if payment

    transaction(requires_new: true) do
      User.where(id: id).update_all update_string
    end
  rescue ActiveRecord::StatementInvalid
    raise LowBalance, "current balance is #{balance}, trying to pay #{-amount}"
  else
    reload
  end
end
