class ApplicationSearch
  include Tainbox

  class <<self
    def equal_filter(column, *args, **options)
      as = options.delete :as
      as ||= column
      attribute(column, *args, **options)
      define_method(:"filter_by_#{column}!") do
        scope.where!(as => public_send(column))
      end
    end

    def substring_filter(column, *args, **options)
      as = options.delete :as
      as ||= column
      attribute(column, *args, **options)
      define_method(:"filter_by_#{column}!") do
        scope.where!("lower(#{as}) LIKE lower(?)", "%#{public_send(column)}%")
      end
    end

    def range_filter(column, *args, **options)
      as = options.delete :as
      as ||= column

      from, till = %w[from till].map { |x| :"#{column}_#{x}" }.tap do |columns|
        columns.each { |x| attribute x, *args }
      end
      define_method(:"filter_by_#{from}!") do
        scope.where!("#{as} >= ?", public_send(from))
      end
      define_method(:"filter_by_#{till}!") do
        scope.where!("#{as} <= ?", public_send(till))
      end
    end
  end

  def initialize(params, scope = nil)
    @scope = scope if scope
    new_attributes = params || {}

    new_attributes.select! { |_k, v| v.present? }
    new_attributes = convert_ids(new_attributes)
    check_attributes! new_attributes

    self.attributes = new_attributes

    tainbox_provided_attributes.each do |attribute|
      public_send :"filter_by_#{attribute}!"
    end
  end

  private

  def check_attributes!(hash)
    unknown_attribute = hash.find { |k, _v| !k.to_sym.in? self.class.tainbox_attributes }
    return unless unknown_attribute
    raise NameError, "Unknown Attribute #{unknown_attribute}"
  end

  def convert_ids(hash)
    hash.map do |key, value|
      key = key.to_s.sub(/_id\z/, '')
      [key, value]
    end.to_h
  end
end
