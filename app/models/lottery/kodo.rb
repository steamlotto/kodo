class Lottery::Kodo < Lottery::Base
  include MountConfig
  store_accessor :result, :numbers, :gun
  store_accessor :metadata, :number

  after_create :generate_number

  class ParticipationValidator < Lottery::ParticipationValidator
    validate :validate_guns
    validate :validate_numbers

    def validate_numbers
      errors.add :numbers, 'Выберете ровно 8 цифр' unless numbers.uniq.length == 8
      numbers.each do |number|
        errors.add :numbers, "Выберете номер от 1 до 30, а не #{number}" unless number.in? 1..30
      end
    end

    def validate_guns
      errors.add :guns, 'Выберете от 1 до 4 модификаторов' unless guns.uniq.length.in? 1..4
      guns.each do |gun|
        errors.add :guns, 'Неизвестный модификатор' unless gun.in? 1..4
      end
    end
  end

  def self.future_count
    31
  end

  def self.numbers_stats
    result = (1..30).map { |x| [x, 0] }.to_h
    pluck("result -> 'numbers'").each do |row|
      row.each { |x| result[x] += 1 }
    end

    result.map do |key, value|
      {
        number: key,
        count: value,
      }
    end
  end

  def self.filter_by_numbers_range(offset: 0, count:)
    ordered = order "(metadata ->> 'number')::integer DESC"

    begin
      running_number = Integer(ordered.first.metadata['number'])
    rescue
      return none
    end

    last =  running_number - offset.to_i
    first = last - count.to_i + 1

    ordered.where("(metadata ->> 'number')::integer BETWEEN ? AND ?", first, last)
  end

  def self.find(id)
    find_by!("metadata ->> 'number' = ?", id)
  end

  def participate!(user, params)
    super do
      user.increase_balance!(-params['bet'].to_f)

      participation = participations.new bet: params['bet'],
                                         user: user,
                                         selection: {
                                           guns: params['guns'].map { |x| Integer(x) },
                                           numbers: params['numbers'].map { |x| Integer(x) },
                                         },
                                         bet_metadata: params.fetch(:bet_metadata, {})
      ParticipationValidator.new(participation)
    end
  end

  protected

  def duration
    @duration = minutes_duration.minutes
  end

  def generate_result!
    self.numbers = (1..30).to_a.sample(8)
    self.gun = SecureRandom.random_number(4) + 1
  end

  def generate_number
    metadata['number'] = self.class.connection.execute(<<-SQL).first['nextval']
      SELECT nextval('lottery_metadata_number_seq');
    SQL
    save!
  end

  def write_lifetime_stat!
    super do |stat|
      numbers.each do |i|
        stat['numbers'][i] += 1
      end
    end
  end
end
