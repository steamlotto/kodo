class Lottery::Kodo::MultiBetProcessor
  include Tainbox
  include Synchronizable

  attribute :user, User

  attribute :factor, Integer
  attribute :circles, Integer
  attribute :count, Integer, default: 1
  attribute :guns, Integer

  attribute :bets

  def process!
    validate_bets!

    synchronize(user_id: user.id) do
      sum = circles * bets.sum { |x| x[:bet] }
      user.increase_balance!(-sum)
      SuperPrize.for(Lottery::Kodo).increase! sum
      bet_metadata = { circles: lotteries.map(&:number) }

      ids = Lottery::Participation.connection.execute <<-SQL
        INSERT INTO lottery_participations(user_id, bet_metadata, created_at, updated_at,
                                           bet, selection, lottery_id)
        (
          SELECT * FROM
          (VALUES (
            #{user.id},
            #{quoted_attribute(:bet_metadata, bet_metadata)}::json,
            now(),
            now()
          )) AS base_attrs(user_id, bet_metadata, created_at, updated_at)
          CROSS JOIN
          (VALUES #{
            bets.map do |bet|
              selection = bet.slice(:guns, :numbers)
              "(#{bet[:bet]}, #{quoted_attribute(:selection, selection)}::json)"
            end.join(',')
          }) AS generated_bet(bet, selection)
          CROSS JOIN
          (VALUES #{lotteries.map { |x| "(#{x.id})"}.join(',')})
            AS lotteries(id)
        )
        RETURNING id;
      SQL

      Kodo::BetProcessor.enqueue(ids.map { |x| x['id'] })
    end
  end

  def bets=(x)
    case x
    when Hash
      x = x.values
      @bets = x.each { |bet| update_bet! bet }
    when Array
      @bets = x.each { |bet| update_bet! bet }
    when String, Integer
      if attribute_provided? :guns
        @bets = x.to_i.times.map { generate_bet! }
      else
        raise ArgumentError, 'you should specify guns if using automated bet'
      end
    end
  end

  def lotteries
    @lotteries ||= pluck_lotteries
  end

  private

  def validate_bets!
    bets.each do |bet|
      validator = Lottery::Kodo::ParticipationValidator.new(
          OpenStruct.new(selection: bet.stringify_keys, bet: bet[:bet])
      )

      return if validator.valid?
      raise "Invalid bet! #{bet}: \n #{validator.errors.full_messages}"
    end
  end

  def quoted_attribute(name, value)
    Lottery::Participation.quoted_attribute(name,value)
  end

  def generate_bet!
    HashWithIndifferentAccess.new.tap do |bet|
      bet[:guns] = (1..4).to_a.sample(guns)
      bet[:numbers] = (1..30).to_a.sample(8)
      update_bet! bet
    end
  end

  def update_bet!(bet)
    bet[:guns].map! { |x| Integer(x) }
    bet[:numbers].map! { |x| Integer(x) }
    bet[:bet] = Lottery::Kodo.ticket_cost * factor * bet[:guns].length
  end

  def pluck_lotteries
    Lottery::Kodo.unfinished
      .where('time_till > ?', Time.zone.now).order(time_from: :asc)
      .limit(circles).tap do |lotteries|
      raise Lottery::LotteryNotAvailable if lotteries.length < circles
    end
  end
end
