class Lottery::ParticipationSearch < ApplicationSearch
  include SearchPagination

  equal_filter :lottery, Lottery::Base
  equal_filter :user
  attribute :date
  attribute :lottery_number
  attribute :lottery_types, default: %w[Kodo Headshot], provide: false
  attribute :gun_matched, :Boolean, strict: false
  attribute :numbers_matched

  def lottery_id
    return unless lottery
    lottery.id
  end

  def scope
    @scope ||= Lottery::Participation.where('user_id IS NOT NULL').joins(:lottery)
  end

  def grouped
    scope
      .group(:user_id)
      .select <<-SQL
        user_id,
        COUNT(*) as count,
        SUM(bet) as bets,
        SUM(win) as wins
    SQL
  end

  def grouped_for_kodo_stats
    grouped
      .group("win_metadata -> 'gun_hits'")
      .group("win_metadata -> 'number_hits'")
      .select <<-SQL
        win_metadata -> 'gun_hits' as gun_hits,
        win_metadata -> 'number_hits' as number_hits
    SQL
  end

  def grouped_for_headshot_stats
    grouped.group(:win)
  end

  def numbers_matched=(array)
    super(array.select(&:present?))
  end

  def filter_by_date!
    as_time = date.to_time.in_time_zone('Moscow')
    scope.where! 'updated_at BETWEEN ? AND ?',
                 as_time.beginning_of_day,
                 as_time.end_of_day
  end

  def filter_by_lottery_number!
    scope.where!("metadata ->> 'number' = ?", lottery_number)
  end

  def filter_by_lottery_types!
    types = lottery_types.select(&:present?).map { |x| "Lottery::#{x.camelize}" }
    scope.where!(lotteries: { type: types })
  end

  def filter_by_gun_matched!
    return if gun_matched.nil?
    scope.where! <<-SQL
      win_metadata -> 'gun_hits' = '#{gun_matched ? 1 : 0}'
    SQL
  end

  def filter_by_numbers_matched!
    return if numbers_matched.empty?
    scope.where! <<-SQL
      win_metadata -> 'number_hits' IN (#{numbers_matched.map { |x| "'#{x}'" }.join(',')})
    SQL
  end
end
