class Lottery::ParticipationValidator
  include ActiveModel::Validations
  attr_accessor :participation

  delegate :bet, to: :participation
  validates :bet, presence: true

  def initialize(participation)
    self.participation = participation
  end

  def method_missing(name, *)
    participation.selection.fetch(name.to_s) { super }
  end

  def save
    return unless valid?
    participation.save
  end
end
