class Lottery::Base < ActiveRecord::Base
  include Synchronizable

  self.table_name = :lotteries

  has_many :participations, class_name: 'Lottery::Participation', foreign_key: :lottery_id
  before_save :apply_default_duration!
  scope :future, -> { where 'time_from > ?', Time.zone.now }
  scope :past, -> { where 'time_till < ?', Time.zone.now }
  scope :finished, -> { where.not result: nil }
  scope :unfinished, -> { where result: nil }

  store_accessor :metadata, :superprize_played, :current_superprize

  def self.running
    time = Time.zone.now
    unfinished.where('time_from <= ?', time).where('time_till >= ?', time).first
  end

  def self.future_count
    1
  end

  def self.require_next?
    future.limit(future_count).count < future_count
  end

  def stat
    self.class::Stat.find_or_create_by(lottery_id: id) { |x| x.date = time_from.to_date }
  end

  def finished?
    !!result
  end

  def to_param
    self.class.name.demodulize.downcase
  end

  def self.singular_route_key
    'lottery'
  end

  def process_end!
    synchronize do
      generate_result!
      award_participations!
      self.superprize_played = super_prize.try(:finalize!)
      save!
      stat.cache!
    end
  end

  def super_prize
    return @super_prize if defined?(@super_prize)
    @super_prize = SuperPrize.for(self)
  end

  def create_next!
    next_lottery = dup
    next_lottery.time_from = time_till
    next_lottery.time_till = nil # see `apply_default_duration!`
    next_lottery.result = nil
    next_lottery.save!
  end

  def participate!(user, _params, &block)
    synchronize do |&result_picker|
      user.update! "#{short_name}_tickets" => user.send(:"#{short_name}_tickets") + 1
      raise if time_till < Time.zone.now
      result_picker.call(participation = block.call)
      participation.save or raise ActiveRecord::Rollback
      process_bet(participation)
    end
  end

  def process_bet(participation)
    stat.try(:push, participation)
  end

  def lifetime_stat
    return @lifetime_stat if defined?(@lifetime_stat)
    @lifetime_stat = Lottery::LifetimeStat.for(self)
  end

  protected

  method_stub :generate_result!, :duration

  def award_participations!
    self.current_superprize = super_prize.try(:amount)
    participations.unprocessed.find_each do |participation|
      self.class::AwardProcessor.new(self, participation).award!
    end
    lifetime_stat.try(:save!)
  end

  def write_lifetime_stat!
    return unless block_given?
    lifetime = Lottery::LifetimeStat.for(self) or return
    # Fuck race conditions, it useless stat, not something important
    yield(lifetime).tap { lifetime.save }
  end

  private

  def short_name
    self.class.name.demodulize.downcase
  end

  def apply_default_duration!
    self.time_till ||= time_from + duration
  end
end
