class Lottery::Participation < ActiveRecord::Base
  extend MassAssignment

  self.table_name = :lottery_participations

  belongs_to :user
  belongs_to :lottery, class_name: 'Lottery::Base'

  scope :unprocessed, -> { where win: nil }
  after_create { lottery.super_prize.try(:process_bet!, self) }

  def loose!(metadata = {})
    update! win: 0, win_metadata: metadata
  end

  def win!(amount: bet, ratio: 1, super_prize: nil, **metadata)
    base = win || 0
    update! win: (base + amount * ratio), win_metadata: metadata
    increase_balance!(win - base)
    if super_prize
      super_prize.process_win! self
    else
      super_prize = lottery.super_prize or return
      super_prize.process_win! self
      super_prize.save!
    end
  end

  def increase_balance!(amount = nil)
    user.increase_balance! amount || win
  end
end
