class Lottery::Headshot < Lottery::Base
  include MountConfig
  after_create :generate_participations

  def stat
    Lottery::Headshot::Stat.find_or_create_by(date: Date.today)
  end

  def duration
    duration_days.days
  end

  def process_end!
    synchronize do
      update! result: { finished: true }, time_till: Time.zone.now
      next_lottery = Lottery::Headshot.future.take!
      next_lottery.time_from = time_till
      next_lottery.time_till = nil
      next_lottery.save!
    end
  end

  def participate!(user, _params)
    super do
      user.increase_balance!(-ticket_cost)

      participations.find_by(user: nil).tap do |participation|
        unless participation
          process_end!
          raise Lottery::LotteryAlreadyFinished
        end
        participation.user = user
        participation.increase_balance!
        stat.append participation
      end
    end
  end

  private

  def generate_participations
    synchronize do
      ParticipationsGenerator.new(self).generate!
    end
  end
end
