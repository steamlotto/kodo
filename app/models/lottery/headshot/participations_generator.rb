class Lottery::Headshot::ParticipationsGenerator
  attr_accessor :bets, :wins, :lottery
  delegate :participations_templates, :participations, to: :lottery
  delegate :min_ratio, :max_ratio, to: :lottery
  delegate :bets_base, :wins_base, to: :lottery
  delegate :ticket_cost, to: :lottery

  def initialize(lottery)
    self.lottery = lottery
  end

  def new_participations
    @new_participations ||= participations_templates.each_with_object([]) do |template, object|
      template = template.symbolize_keys
      count = template.delete :count
      count.times do |_i|
        object << OpenStruct.new(bet: ticket_cost, **template)
      end
    end
  end

  def swap(i, j)
    return if i == j
    new_participations[i], new_participations[j] = new_participations[j], new_participations[i]
  end

  def win_window
    Range.new(*[min_ratio, max_ratio].map { |x| bets * x - wins - ticket_cost })
  end

  def normalize_par!(index)
    Rails.logger.info "creating #{index} participation with win_window #{win_window}"

    good_index = index + new_participations[index..-1].find_index { |x| x.win.in? win_window }
    swap(index, good_index)
    new_participations[index]
  end

  def generate!
    new_participations.shuffle!
    self.bets = bets_base.to_f
    self.wins = wins_base.to_f
    create_participations!
  end

  def create_participations!
    (0..(new_participations.size - 1)).each do |i|
      par = normalize_par!(i)
      self.bets += par.bet
      self.wins += par.win
      participations.create! par.to_h
    end
  end
end
