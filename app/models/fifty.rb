class Fifty < ActiveRecord::Base
  belongs_to :user

  def self.active
    find_by game_done: false
  end

  def self.find_id(id)
    find_by id: id
  end

  def self.make_bet(user, chosen)
    sim = rand(2)
    if sim == 0
      sim = -1
    end

    new_history = user.fifties.active.history.reverse
    new_history[user.fifties.active.current_step] = sim
    user.fifties.active.update! history: new_history.reverse!

    if sim == chosen
      user.fifties.active.update! current_step: user.fifties.active.current_step + 1
      user.fifties.active.update! run_at: Time.now
    else
      user.fifties.active.update! game_done: true
    end
  end

  def self.take_bet(user)
    amount = 0
    if user.fifties.active.factor != 0
      round = user.fifties.active.current_step
      reverse = user.fifties.active.win_amount.reverse
      amount = reverse[round]
      FiftyHigh.participate(user.fifties.active.current_step, user.fifties.active.run_at)
    end
    user.increase_balance!(amount)
    user.fifties.active.update! game_done: true
  end

  def self.participate(user, factor)
    transaction do
      case factor
        when 0
          user.fifties.create(current_step: 0, factor: factor, game_done: false, win_amount: [45570, 22785, 11265, 5635, 2785, 1390, 690, 345, 170, 100], run_at: Time.now)
        when 10
          user.fifties.create(current_step: 0, factor: factor, game_done: false, win_amount: [4456, 2278, 1125, 562, 278, 140, 68, 34, 17, 10], run_at: Time.now)
        when 30
          user.fifties.create(current_step: 0, factor: factor, game_done: false, win_amount: [13670, 6835, 3380, 1690, 835, 415, 206, 103, 51, 30], run_at: Time.now)
        when 50
          user.fifties.create(current_step: 0, factor: factor, game_done: false, win_amount: [22800, 11250, 5670, 2750, 1360, 680, 340, 170, 85, 50], run_at: Time.now)
        when 100
          user.fifties.create(current_step: 0, factor: factor, game_done: false, win_amount: [45570, 22785, 11265, 5635, 2785, 1390, 690, 345, 170, 100], run_at: Time.now)
        else
      end
      user.increase_balance!(-factor)
    end
  end
end