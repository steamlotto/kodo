class ItemClass < ActiveRecord::Base
  has_many :items, counter_cache: true

  validates :price, presence: { message: 'Укажите цену, чтобы включить' }, if: :enabled?
  validates :best, absence: { message: 'Включите акцию, чтобы добавить в список лучших' },
                   unless: :enabled?

  scope :enabled, -> { where(enabled: true) }

  def self.for_admin
    select <<-SQL
      item_classes.*,
      (
        SELECT COUNT(*) FROM items
        WHERE items.item_class_id = item_classes.id
          AND bot_order_id IS NULL
      ) as items_count
    SQL
  end

  before_save :cache_additional_attributes

  private

  def cache_additional_attributes
    self.stared = (weapon_type == 'Нож')
    self.stattrak = !!(market_hash_name =~ /\AStatTrak/)
    true
  end
end
