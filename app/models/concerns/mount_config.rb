module MountConfig
  extend ActiveSupport::Concern

  module ClassMethods
    attr_accessor :config_file

    def config_data
      @config_data ||=
          begin
            file = config_file || "#{model_name.config_path}.yml"
            data = YAML.load ERB.new(File.read(file)).result
            env = ENV['RAILS_ENV']
            data.tap { data.merge! data[env] if data.key?(env) }
          end
    end

    def method_missing(name, *)
      config_data.fetch(name.to_s) { super }
    end

    def respond_to_missing?(name, *)
      config_data.key?(name.to_s) || super
    end
  end

  def method_missing(name, *)
    config_data.fetch(name.to_s) { super }
  end

  def respond_to_missing?(name, *)
    config_data.key?(name.to_s) || super
  end

  private

  def config_data
    self.class.config_data
  end
end
