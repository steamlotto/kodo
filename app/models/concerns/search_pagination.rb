module SearchPagination
  extend ActiveSupport::Concern

  included do
    attribute :count, Integer
    attribute :offset, Integer
  end

  def filter_by_count!
    scope.limit!(count)
  end

  def filter_by_offset!
    scope.offset!(offset)
  end
end
