module MassAssignment
  def quoted_attribute(name, value)
    connection.quote(value, column_for_attribute(name))
  end
end
