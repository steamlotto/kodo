class Bot < ActiveRecord::Base
  def self.new(steam_login,steam_password,email_login,email_password)
    Bot.create!(login_steam: steam_login, pass_steam:steam_password, login_mail:email_login, pass_mail: email_password)
  end
end
