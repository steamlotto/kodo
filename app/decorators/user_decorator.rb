class UserDecorator < ApplicationDecorator
  decorates User

  delegate :admin_scope_access?, to: :role

  def as_json(*)
    super(only: %i[id balance nickname steam_id steam_offer_link sound avatar]).merge(
      admin_scope_access: admin_scope_access?,
      created_at: model.created_at.in_time_zone('Moscow'),
      kodo_tickets: kodo_tickets,
    )
  end

  def guest?
    !persisted?
  end

  def name
    nickname
  end
end
