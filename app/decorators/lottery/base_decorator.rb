class Lottery::BaseDecorator < ApplicationDecorator
  def as_json(*)
    super(
      only: %i[result],
      methods: %i[id current_superprize superprize_played],
    ).merge(
      time_from: model.time_from.in_time_zone('Moscow'),
      time_till: model.time_till.in_time_zone('Moscow'),
    )
  end

  def id
    metadata.fetch('number', model.id)
  end

  def finished_at
    h.l model.time_till.in_time_zone('Moscow')
  end
end
