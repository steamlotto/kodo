class Lottery::Headshot::ParticipationDecorator < ApplicationDecorator
  def as_json(*)
    super(
        only: %i[id user_id lottery_id],
        methods: %i[updated_at win]
    )
  end

  def win
    "#{model.win.to_i} зол."
  end

  def updated_at
    model.updated_at.in_time_zone('Moscow')
  end
end
