class Lottery::StatDecorator < ApplicationDecorator
  decorates_association :lottery

  # Use implicit delegation because it decorates grouped stats

  %i[winners wins].each do |name|
    define_method(name) do
      if_finished { model.public_send(name) }
    end
  end

  def lottery_id
    lottery.id
  end

  def finished?
    model.try(:lottery) ? lottery.finished? : true
  end

  def date
    h.link_to h.l(model.date), [:by_date, *path, :index, date: model.date]
  end

  def profit
    if_finished { model.bets * (1 - superprize_ratio) }
  end

  def jackpot_change
    if_finished { model.bets - model.wins - profit }
  end

  def path
    selection = model.class.name.split('::')[-2].downcase
    [:admin, :lotteries, selection]
  end

  private

  def superprize_ratio
    @superprize_ratio = SuperPrize.for(Lottery::Kodo).ratio
  end

  def if_finished
    raise ArgumentError unless block_given?

    finished? ? yield : h.hs(:mdash)
  end
end
