class ItemClassDecorator < ApplicationDecorator
  def as_json(*)
    super(
        only: %i[id name price best rareness weapon_type classfield stared stattrak items_count],
        methods: %i[preview]
    )
  end

  def preview
    model.best? ? icon(290, 150) : icon(207, 90)
  end

  def icon(x, y)
    "http://steamcommunity-a.akamaihd.net/economy/image/#{model.icon}/#{x}fx#{y}f"
  end
end
