class Admin::ItemClassesController < Admin::AdminController

  def index
    @search = ItemClassSearch.new(params[:search], ItemClass.for_admin)
    @form = Admin::ItemClassesForm.multi_form(@search.scope)
  end

  def reload
    BotOrder::Reload.create!
    flash[:notice] = 'Заявка отправленна'
    redirect_to [:admin, :item_classes]
  end

  def update_all
    @search = ItemClassSearch.new(params[:search], ItemClass.for_admin)
    @form = Admin::ItemClassesForm.multi_form(@search.scope)

    if @form.submit params.require(:item_class)
      redirect_to [:admin, :item_classes, search: params[:search]], notice: 'Готово!'
    else
      flash.now[:fail] = 'Ошибка!'
      render :index
    end
  end

end
