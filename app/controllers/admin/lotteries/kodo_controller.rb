class Admin::Lotteries::KodoController < Admin::AdminController
  def index
    @stats = Lottery::StatDecorator.decorate_collection \
      Lottery::Kodo::Stat.group_by_date.order(date: :desc)
  end

  def by_date
    date = params[:date].to_date rescue render_404
    @stats = Lottery::Kodo::Stat.where(date: date).includes(:lottery)
             .order("lotteries.metadata ->> 'number' DESC")
    @stats = Lottery::StatDecorator.decorate_collection @stats
    render :index
  end

  def show
    @lottery = Lottery::Kodo.find(params[:id]).decorate
    @stats = Lottery::ParticipationSearch.new(lottery: @lottery)
             .grouped_for_kodo_stats
  end
end
