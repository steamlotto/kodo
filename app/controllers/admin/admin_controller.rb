class Admin::AdminController < ActionController::Base
  include UserSession

  layout 'admin'

  private

  def admin_root_path
    url_for [:admin, :item_classes]
  end
end
