class Admin::TestersController  < Admin::AdminController

  def index
    @jobs=QueJob.all
  end

  def item_parser_add
    QueJob.create!(priority:100, job_class:'ITEM', args:params[:args].to_json, user_id: params[:user][0..-1])
    render json: {status:'complite'}
  end
end
