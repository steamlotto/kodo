class Admin::TicketsController < Admin::AdminController
  before_action :find_ticket, except: %i[index]

  def index
    @search = TicketSearch.new(params[:search])
    @tickets = @search.scope.order(created_at: :asc)
  end

  %i[open close].each do |x|
    define_method(x) do
      @ticket.tap(&:"#{x}!").save!
      redirect_to [:admin, :tickets]
    end
  end

  def show
    @ticket = @ticket.decorate
  end

  # def reply
  #   @comment = @ticket.comments.new(user: current_user)
  #   @comment.update_attributes params.require(:ticket_comment).permit(:body)
  #   if @comment.save
  #     redirect_to [:admin, @ticket]
  #   else
  #     render :show
  #   end
  # end

  private

  def find_ticket
    @ticket = Ticket.find(params[:id])
  end
end
