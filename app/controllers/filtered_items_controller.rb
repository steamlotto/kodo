class FilteredItemsController < ApplicationController

  def getfilter
    q = ItemClass.select(:rareness).distinct
    t = ItemClass.select(:weapon_type).distinct
    c = ItemClass.select(:classfield).distinct

    qualities = [];
    qualities.push('Все');
    q.each do |quality|
      qualities.push(quality.rareness)
    end

    types = [];
    types.push('Все');
    t.each do |type|
      types.push(type.weapon_type);
    end

    classes = [];
    classes.push('Все');
    c.each do |cl|
      classes.push(cl.classfield)
    end

    render json: {qualities: qualities, types: types, classes: classes, pages: [1,2]}
  end

  def showpage
    page = 1
    size = 10
    if params[:page]
      page = params[:page];
    end
    pagedItems = Item_Class.limit(size).offset((page-1)*size)
  end
end