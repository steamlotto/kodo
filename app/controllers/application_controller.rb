class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :set_headers

  include UserSession

  def head_ok
    head :ok
  end

  private

  def redirect_to(*args)
    return super unless request.xhr?

    path = url_for(*args)

    render js: <<-JAVASCRIPT
        window.location = '#{path}';
    JAVASCRIPT
  end

  def render_404(message = 'Not found')
    raise ActionController::RoutingError, message
  end

  def set_headers
    # Disable caching
    # headers['Last-Modified'] = Time.now.httpdate

    # In memory of sir Terry Pratchett
    headers['X-Clacks-Overhead'] = 'GNU Terry Pratchett'
  end
end
