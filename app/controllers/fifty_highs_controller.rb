class FiftyHighsController < ApplicationController

  def show
    render json: {active: FiftyHigh.take_one}
  end

end