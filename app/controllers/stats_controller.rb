class StatsController < ApplicationController
  def show
    render json: {
      last_kodo: last_kodo.decorate,
      kodo_stats: kodo_stats,
      kodo_bets: kodo_bets,
      long_been_seen: long_been_seen,
      superprize_current: superprize_current,
    }.compact
  end

  def superprize_current
    @superprize_current=SuperPrize.last['amount']
  end
  def last_kodo
    @last_kodo = Lottery::Kodo.finished.filter_by_numbers_range(count: 10)
  end

  def long_been_seen
  end

  def kodo_stats
    {
      last: last_kodo.numbers_stats,
      all: Lottery::LifetimeStat.for(Lottery::Kodo)['numbers']
        .map { |k, v| { number: k, count: v['count'], last_seen: v['last_seen'] } },
    }
  end

  def kodo_bets
    return if current_user.guest?
    Lottery::Kodo::ParticipationDecorator.decorate_collection \
      Lottery::ParticipationSearch.new(user: current_user, lottery_types: %w[Kodo])
      .scope.order(created_at: :desc).limit(2)
  end

  def headshot_bets
    return if current_user.guest?
    Lottery::Headshot::ParticipationDecorator.decorate_collection \
      Lottery::ParticipationSearch.new(user: current_user, lottery_types: %w[Headshot])
      .scope.order(updated_at: :desc).limit(2)
  end
end
