class RobokassaController < ApplicationController
  skip_before_action :verify_authenticity_token, except: :signer

  def result
    id = params.require 'InvId'
    Rails.logger.info "Robokassa confirmed payment id=#{id}"
    Robokassa.process_result! params

    head :ok
  end

  def success
    if params[:InvId] && params[:SignatureValue]
      Rails.logger.info "Robokassa confirmed payment id=#{id}"
      if !params[:SHPUid]
        params[:SHPUid] = current_user;
      end
      Robokassa.process_result! params
      render text: "Success"
    end
    else
      raise SecurityError, "Invoice mismatch";
  end

  def fail
    render text: 'Ошибка оплаты'
  end

  def signer
    redirect_to Robokassa.new(amount: params.require(:amount), user_id: current_user.id).to_url
  end
  
  def gotourl
    @amount = params[:amount]
    @roborequest = Robokassa.new(user_id: current_user.id, amount:@amount);
    render json: { merchant_login: @roborequest.merchant_login, 
          sum: @roborequest.out_sum,
          culture: @roborequest.culture,
          encoding: @roborequest.encoding,
          invoice: @roborequest.invoice_id,
          shpid: @roborequest.shp_uid,
          signature_sum: @roborequest.signature_value}
  end
end
