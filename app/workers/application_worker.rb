Signal.trap('TERM') do
  ApplicationWorker.stop
  exit 0
end

class ApplicationWorker
  attr_reader :updated
  alias_method :updated?, :updated

  def self.stop
    @@singleton.stop
  end

  def initialize
    @@singleton = self
  end

  def logger_name
    self.class.name.downcase
  end

  def delay
    1.minute
  end

  def updated!
    @updated = true
  end

  def stop; end

  def run!
    set_logger!
    loop do
      begin
        run
        sleep delay unless updated?
      rescue => e
        raise e if ENV['DEBUG']
        ExceptionNotifier.notify_exception(e)
        sleep delay
      ensure
        @updated = false
      end
    end
  ensure
    stop
  end

  private

  def set_logger!
    logger =  Logger.new(Rails.application.logs_path.join "#{logger_name}.log")
    Rails.logger = logger
    ActiveRecord::Base.logger = logger
  end
end
