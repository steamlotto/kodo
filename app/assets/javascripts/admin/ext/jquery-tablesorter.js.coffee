$ ->
  textExtraction = (e) ->
    element = $(e)
    element.data('sort-by') || element.text()
  $('table.tablesorter').tablesorter textExtraction: textExtraction
