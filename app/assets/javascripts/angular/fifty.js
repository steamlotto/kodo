app.controller('fiftyCtrl', function ($rootScope, $interval, $timeout) {
    var fifty = this;

    fifty.chosen = 0;

    fifty.leftStatus = '';
    fifty.rightStatus = '';
    fifty.messageField = 'Начните игру!';
    fifty.high = '0';


    fifty.gameStartTime = undefined;
    fifty.buttonField = 'Старт';
    fifty.backgroundTimer = 0;
    fifty.timer = 0;
    fifty.timerShow = 0;
    fifty.currentRound = 0;
    fifty.game_done = true;
    fifty.enter_cost = 0;
    fifty.currentTime = undefined;

    fifty.rework__me = function (item) {
        if (item == 'демо') {
            fifty.enter_cost = 0;
        } else {
            fifty.enter_cost = item;
        }
    };

    var lottery__input = function (data) {
        this.available = data.available;
        this.initial_value = data.initial_value;
        this.reset();
    };
    lottery__input.prototype = {
        reset: function () {
            this.value = this.initial_value || this.available[4];
        },
        inc: function () {
            var id = this.available.indexOf(this.value) + 1;
            if (id < this.available.length) {
                this.value = this.available[id];
                if (this.available[id] == 'демо') {
                    fifty.enter_cost = 0;
                } else {
                    fifty.enter_cost = this.available[id];
                }
                fifty.refreshData();
            }
        },
        dec: function () {
            var id = this.available.indexOf(this.value) - 1;
            if (id >= 0) {
                this.value = this.available[id];
                if (this.available[id] == 'демо') {
                    fifty.enter_cost = 0;
                } else {
                    fifty.enter_cost = this.available[id];
                }
                fifty.refreshData();
            }
        }
    };

    fifty.factor = new lottery__input({available: [10, 30, 50, 100, 'демо']});

    fifty.table = [
        {win: 45570, left: 0, right: 0, time: 22000},
        {win: 22785, left: 0, right: 0, time: 20000},
        {win: 11265, left: 0, right: 0, time: 18000},
        {win: 5635, left: 0, right: 0, time: 16000},
        {win: 2785, left: 0, right: 0, time: 14000},
        {win: 1390, left: 0, right: 0, time: 12000},
        {win: 690, left: 0, right: 0, time: 10000},
        {win: 345, left: 0, right: 0, time: 8000},
        {win: 170, left: 0, right: 0, time: 6000}
    ];

    fifty.choseLeft = function () {
        fifty.messageField = 'Вы уверены?';
        fifty.leftStatus = 'chosen';
        fifty.rightStatus = '';
        fifty.chosen = 1
    };

    fifty.choseRight = function () {
        fifty.messageField = 'Вы уверены?';
        fifty.leftStatus = '';
        fifty.rightStatus = 'chosen';
        fifty.chosen = -1
    };

    var timeFun;

    fifty.refreshData = function () {
        if (fifty.game_done == true) {
            $timeout(function () {
                fifty.messageField = 'Начните игру!';
            }, 1000);
            fifty.buttonField = 'Начните игру';
            if (angular.isDefined(timeFun)) {
                $interval.cancel(timeFun);
                timeFun = undefined;
            }
        } else {
            if (fifty.leftStatus == '' && fifty.rightStatus == '') {
                fifty.messageField = 'Скорее выбирайте ящик!';
            }
            if (fifty.enter_cost == 0) {
                fifty.buttonField = 'Это демо версия'
            } else {
                if (fifty.currentRound == 0) {
                    fifty.buttonField = 'Забрать ' + fifty.enter_cost + ' золотых'
                } else {
                    fifty.buttonField = 'Забрать ' + fifty.table[fifty.table.length - fifty.currentRound].win + ' золотых'
                }
            }
            var date1 = new Date(fifty.currentTime);
            var date2 = new Date(fifty.gameStartTime);
            fifty.timer = fifty.table[fifty.table.length - 1 - fifty.currentRound].time - date1.getTime() + date2.getTime();
            if (!angular.isDefined(timeFun)) {
                timeFun = $interval(function () {
                    if (fifty.timer >= 0) {
                        fifty.timer = fifty.timer - 500;
                        if (fifty.timer >= 0) {
                            fifty.timerShow = Math.floor(fifty.timer / 1000)
                        }
                    } else {
                        $.post('/fifties/bet?chosen=' + fifty.chosen)
                            .success(function (data) {
                                $rootScope.$emit('updateUser', data['current_user']);
                                fifty.fiftyStatus();
                                $interval.cancel(timeFun);
                                timeFun = undefined;

                                for (var j = 0; j < 8 - fifty.currentRound; j++) {
                                    if (data.active.history[j] == 0) {
                                        fifty.table[j].left = 0;
                                        fifty.table[j].right = 0;
                                    }
                                }

                                fifty.leftStatus = '';
                                fifty.rightStatus = '';

                                if (data.active.history[8 - fifty.currentRound] == 1 && fifty.chosen == 1) {
                                    fifty.table[8 - fifty.currentRound].left = 1;
                                    fifty.table[8 - fifty.currentRound].right = 0;
                                    fifty.changeBackground('green__select', 'left');
                                    fifty.messageField = 'Вы угадали!';
                                }
                                if (data.active.history[8 - fifty.currentRound] == -1 && fifty.chosen == -1) {
                                    fifty.table[8 - fifty.currentRound].right = 1;
                                    fifty.table[8 - fifty.currentRound].left = 0;
                                    fifty.changeBackground('green__select', 'right');
                                    fifty.messageField = 'Вы угадали!';
                                }
                                if (data.active.history[8 - fifty.currentRound] == -1 && fifty.chosen == 1) {
                                    fifty.table[8 - fifty.currentRound].right = 3;
                                    fifty.table[8 - fifty.currentRound].left = 2;
                                    fifty.changeBackground('red', 'right');
                                    fifty.messageField = 'Вы не угадали.';
                                }
                                if (data.active.history[8 - fifty.currentRound] == 1 && fifty.chosen == -1) {
                                    fifty.table[8 - fifty.currentRound].right = 2;
                                    fifty.table[8 - fifty.currentRound].left = 3;
                                    fifty.changeBackground('red', 'left');
                                    fifty.messageField = 'Вы не угадали.';
                                }
                                if (data.active.history[8 - fifty.currentRound] == -1 && fifty.chosen == 0) {
                                    fifty.table[8 - fifty.currentRound].right = 3;
                                    fifty.table[8 - fifty.currentRound].left = 0;
                                    fifty.changeBackground('red', 'right');
                                    fifty.messageField = 'Вы не угадали.';
                                }
                                if (data.active.history[8 - fifty.currentRound] == 1 && fifty.chosen == 0) {
                                    fifty.table[8 - fifty.currentRound].right = 0;
                                    fifty.table[8 - fifty.currentRound].left = 3;
                                    fifty.changeBackground('red', 'left');
                                    fifty.messageField = 'Вы не угадали.';
                                }


                                for (var i = 9 - fifty.currentRound; i < 9; i++) {
                                    if (data.active.history[i] == 0) {
                                        fifty.table[i].left = 0;
                                        fifty.table[i].right = 0;
                                    }

                                    if (data.active.history[i] == 1) {
                                        fifty.table[i].left = 1;
                                        fifty.table[i].right = 0;
                                    }
                                    if (data.active.history[i] == -1) {
                                        fifty.table[i].right = 1;
                                        fifty.table[i].left = 0;
                                    }
                                }
                                fifty.chosen = 0;
                            })
                    }
                }, 500)
            }
        }
    };

    fifty.dropStyles = function () {
        if (fifty.leftStatus == 'red' || fifty.leftStatus == 'green__select') {
            fifty.leftStatus = '';
        }
        if (fifty.rightStatus == 'red' || fifty.rightStatus == 'green__select') {
            fifty.rightStatus = '';
        }
    };

    fifty.changeBackground = function (style, side) {
        if (style == 'red' && side == 'left') {
            if ($rootScope.user.sound) {
                document.getElementById('lose_sound').play();
            }
            fifty.leftStatus = 'red';
            fifty.rightStatus = '';
        }
        if (style == 'red' && side == 'right') {
            if ($rootScope.user.sound) {
                document.getElementById('lose_sound').play();
            }
            fifty.leftStatus = '';
            fifty.rightStatus = 'red';
        }
        if (style == 'green__select' && side == 'left') {
            if ($rootScope.user.sound) {
                document.getElementById('win_sound').play();
            }
            fifty.leftStatus = 'green__select';
            fifty.rightStatus = '';
        }
        if (style == 'green__select' && side == 'right') {
            if ($rootScope.user.sound) {
                document.getElementById('win_sound').play();
            }
            fifty.leftStatus = '';
            fifty.rightStatus = 'green__select';
        }
        if (style == 'green__select') {
            $timeout(function () {
                if (fifty.leftStatus == '' && fifty.rightStatus == '') {
                    fifty.messageField = 'Скорее выбирайте ящик!';
                }
            }, 1500);
        }
        $timeout(fifty.dropStyles, 1000)
    };

    fifty.fiftyStatus = function () {
        $.get('/fifties')
            .success(function (data) {
                if (data.active != null) {
                    if (fifty.game_done == true && $rootScope.user.sound) {
                        document.getElementById('start_sound').play();
                    }
                    fifty.currentRound = data.active.current_step;
                    fifty.game_done = data.active.game_done;
                    for (var i = 0; i < 9; i++) {
                        fifty.table[i].win = data.active.win_amount[i]
                    }
                    fifty.gameStartTime = data.active.run_at;
                    fifty.currentTime = data.current_time;
                    fifty.enter_cost = data.active.factor;
                } else {
                    fifty.currentRound = 0;
                    fifty.game_done = true;
                }
                fifty.refreshData();
            })
    };

    fifty.gameButton = function () {

        $.post('/fifties?factor=' + fifty.enter_cost)
            .success(function (data) {
                if (fifty.leftStatus == '' && fifty.rightStatus == '') {
                    fifty.messageField = 'Скорее выбирайте ящик!';
                }
                $rootScope.$emit('updateUser', data['current_user']);
                fifty.fiftyStatus();
            })
    };

    fifty.getHigh = function () {
        $.get('/fifty_high')
            .success(function (data) {
                fifty.high = data.active.high
            })
    };
    fifty.getHigh();


    fifty.fiftyStatus();

}).directive('fiftyInput', function () {
    return {
        restrict: 'C',
        templateUrl: assets.views['fifty__input.html'],
        scope: true,
        link: function (scope, _element, attributes) {
            scope.name = attributes.name;
            scope.prompt = attributes.prompt != 'false';
        }
    }
});