app.controller('bannerCtrl', function ($scope, $rootScope, $interval) {
	var banner = this;
	this.move = false;

    this.kodo = (function() {
        var _kodo = function() {
            this.update();
            this.participations = new this._participations();

            this.timerInterval = $interval(this.calculateTime.bind(this), 100);
            this.catchInterval = $interval(this.catchLottery.bind(this), 5000);
        };

        _kodo.prototype = {
            _participations: (function() {
                var proto = function() {
                    this._update();
                    $rootScope.$on('kodoBet', this.update.bind(this))
                };

                proto.prototype = {
                    _update: function() {
                        var th = this;

                        window.current_user(function(user)  {
                            if (user.logged_in) {
                                th.update()
                            }
                        });
                    },
                    update: function() {
                        var pars = this;
                        $.get('lotteries/kodo/participations/today')
                            .done(function(data) {
                                pars.participations = data.participations;
                            })
                            .error(function(jqXHR) {
                                var status = jqXHR.status;
                                if(status == 401) {
                                    $rootScope.$emit('updateUser');
                                    pars.participations = [];
                                } else if(status == 404)  {
                                    pars.participations = [];
                                } else {
                                    reportError(jqXHR.responseText)
                                }
                            })
                    },
                    any: function() {
                        if(this.participations) {
                            return this.participations.length > 0;
                        } else {
                            return false
                        }
                    }
                };

                return proto
            }()),
            update: function() {
                $.get('lotteries/kodo')
                    .done(function(data) {
                        this._update(data['lotteries'][0]);
                        this.updateTimes(data['current_time']);
                        $rootScope.$emit('updateUser', data)
                    }.bind(this))
                    .error(function(jqXHR) {
                        var status = jqXHR.status;
                        if(status == 404) {
                            this.running = false;
                        } else {
                            reportError(jqXHR.responseText);
                        }
                    }.bind(this))
            },
            _update: function(data)  {
                this.running = true;
                if (this.id && (this.id != data.id)) {
                    angular.element($('.broadcast')).scope().$emit('showBroadcast', this.id)
                }
                load_data(this, data);
                this.time_from = Date.parse(data.time_from);
                this.time_till = Date.parse(data.time_till);
            },
            updateTimes: function(server_time) {
                var diff = new Date() - Date.parse(server_time);
                if(this.time_from) {
                    this.time_from = this.time_from + diff;
                }
                if(this.time_till) {
                    this.time_till = this.time_till + diff;
                }
            },
            catchLottery: function() {
                if(!this.running) { this.update() }
            },

            calculateTime: function () {
                if(!this.running) {
                    $('.banner__kodo__timer').text('--:--');
                    return;
                }

                var time_till = this.time_till - new Date();
                time_till = (time_till - time_till % 1000) / 1000;

                if (time_till > 0) {
                    var minutes = (time_till - time_till % 60) / 60,
                        seconds = time_till % 60;
                    $('.banner__kodo__timer').text(num2s(minutes, 2) + ':' + num2s(seconds, 2));
                } else {
                    this.running = false
                }
            }
        };

        return new _kodo();
    }());

	$scope.$on('moveToggle', function (event, args) {
		banner.move = args;
	});
});
