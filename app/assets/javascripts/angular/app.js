var app = angular.module('lotteryApp', ['ngRoute']);

//routing
app.config(function($routeProvider){
	$routeProvider
        .when('/',{
            templateUrl: assets.views['main__page.html']
        })
        .when('/indevelop',{
            templateUrl: assets.views['indevelop.html']
        })
        .when('/kodo',{
            templateUrl: assets.views['lottery.html']
        })
        .when('/callback',{
            templateUrl: assets.views['callback.html']
        })
        .when('/ruleskodo',{
            templateUrl: assets.views['ruleskodo.html']
        })
        .when('/rulesheadshot',{
            templateUrl: assets.views['rulesheadshot.html']
        })
        .when('/rulesshop',{
            templateUrl: assets.views['rulesshop.html']
        })
        .when('/archive',{
            templateUrl: assets.views['archive.html']
        })
        .when('/history',{
            templateUrl: assets.views['history.html']
        })
        .when('/store',{
            templateUrl: assets.views['store.html']
        })
        .when('/headshot',{
            templateUrl: assets.views['headshot.html']
        })
        .when('/fifty',{
            templateUrl: assets.views['fifty.html']
        })
        .when('/stone', {
            templateUrl: assets.views['stone.html']
        })
        .when('/rulesfifty',{
            templateUrl: assets.views['rulesfifty.html']
        })
        .otherwise({
            templateUrl: assets.views['error.html']
        });
});