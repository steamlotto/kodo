app.controller('lotteryCtrl', function ($http, $rootScope, $filter, $scope) {
    var lottery = this;
    var _bet = function() {
        this.numbers = [];
        this.guns = [];
    };
    _bet.prototype = {
        filters: {
            even: function(x) { return x%2; },
            odd: function(x) { return !(x%2); },
            top: function(x) { return x <= 15; },
            bottom: function(x) { return x > 15; }
        },
        load: function() {
            this.numbers = this._load(lottery.numbers);
            this.guns = this._load(lottery.guns);
        },
        _load: function(array) {
            return $filter('filter')(array, { selected: true })
                .map(function(x) { return x.index })

        },
        unload: function() {
            this._unload(lottery.numbers, this.numbers);
            this._unload(lottery.guns, this.guns);
        },
        _unload: function(target, source) {
            $.each(target, function() {
                this.selected = !!~(source.indexOf(this.index))
            })
        },
        randomize: function(filter) {
            var arr = lottery.numbers.map(function(x) { return x.index; });
            this._randomize($filter('filter')(arr, this.filters[filter]));
            this.guns = [randomInt(1, 4)];
            this.unload()
        },
        _randomize: function(array) {
            this.numbers =  [];
            for(var i = 0; i< 8; i++) {
                this.numbers.push(array.splice(randomNumber(0, array.length - 1), 1)[0]);
            }
        },
        valid: function() {
            if(this.guns.length < 0 ) { return false; }
            if(this.numbers.length != 8 ) { return false; }
            return true;
        }
    };

    var _number = function(index) {
        this.index = index;
        this.selected = false;
    };
    _number.prototype = {
        toggle: function() {
            if(this.selected) {
                this.selected =  false
            } else {
                if(lottery.bet().numbers.length < 8) {
                    this.selected = true;
                } else {
                    return;
                }
            }
            lottery.bet().load();
        }
    };

    var lottery__input = function(data) {
        this.available = data.available;
        this.initial_value = data.initial_value;
        this.reset();
    };
    lottery__input.prototype = {
        reset: function() {
            this.value = this.initial_value || this.available[0];
        },
        inc: function() {
            var id = this.available.indexOf(this.value) + 1;
            if(id < this.available.length) {
                this.value = this.available[id]
            }
        },
        dec: function() {
            var id = this.available.indexOf(this.value) - 1;
            if(id >= 0) {
                this.value = this.available[id]
            }
        }
    };


    this.type = 'hand';

    this.bet_id = 0;
    this.bets = [new _bet()];
    this.bet = function () {
        return lottery.bets[lottery.bet_id];
    };
    this.cleanBet = function() {
        (lottery.bets[lottery.bet_id] = new _bet()).unload();
    };

    this.addBet = function() {
        this.bets.push(new _bet());
        this.setBet(this.bets.length - 1);
    };

    this.setBet = function(id) {
        this.bet_id = id;
        this.bet().unload();
    };

    this.removeBet = function(id) {
        this.bets.splice(id, 1);
        if(id == this.bet_id) {
            if(id == 0) {
                if(this.bet.length == 1) {
                    this.cleanBet()
                }
            }
        } else {
            if(id == 0) {
                this.setBet(0);
            } else {
                this.setBet(id - 1);
            }
        }
    };

    lottery.flaq = false;
    this.addMoney = function() {
      lottery.flaq = true;
    };

    this.robokassa = function(gold_amount) {
        var newWindow = window.open("", '_blank', 'width');
        $.get('/robokassa/gotourl?amount=' + gold_amount)
            .success(function (data) {
                newWindow.location.href = 'http://auth.robokassa.ru/Merchant/Index.aspx?MerchantLogin=' + data.merchant_login + '&SignatureValue=' + data.signature_sum + '&Culture=ru' + '&OutSum=' + data.sum + '&Encoding=' + data.encoding + '&InvoiceID=' + data.invoice + '&SHPUid=' + data.shpid
            })
    };

    this.clearAll = function() {
        this.bets = [];
        this.bet_id = 0;
        this.cleanBet();
        $.each(this.options, function() { this.reset; });
    };

    this.guns = dup(guns);
    for (var i = 0; i < this.guns.length; i++) {
        this.guns[i].toggle = function () {
            this.selected = !this.selected;
            lottery.bet().load();
        }
    }

    this.multi = {
        total_bets: function() {
            return lottery.options.bets.value * lottery.options.circles.value;
        },
        total_cost: function() {
            return lottery.multi.total_bets() * lottery.options.guns.value *
                lottery.options.factor.value * 30;
        },
        sendData: function() {
            var options = lottery.options;
            return {
                factor: options.factor.value,
                circles: options.circles.value,
                bets: options.bets.value,
                guns: options.guns.value
            }
        }
    };

    this.hand = {
        total_bets: function() {
            return lottery.bets.length * lottery.options.circles.value;
        },
        total_cost: function() {
            var sum = 0;
            $.each(lottery.bets, function() {
                sum += this.guns.length * 30;
            });
            return sum * lottery.options.factor.value * lottery.options.circles.value;
        },
        sendData: function() {
            var options = lottery.options;
            return {
                bets: lottery.bets.map(function(bet) { return { guns: bet.guns, numbers: bet.numbers } }),
                factor: options.factor.value,
                circles: options.circles.value
            }
        },
        valid: function() {
            for(var i=0; i < lottery.bets.length; i++) {
                if(!lottery.bets[i].valid()) { return false; }
            }
            return true;
        }
    };

    this.numbers = [];
    for (var i = 0; i < 30; i++) { this.numbers.push(new _number(i + 1)); }

    this.options = {
        bets: new lottery__input({
            available: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 40, 60, 80, 90, 100],
            initial_value: 10
        }),
        guns: new lottery__input({ available: [1, 2, 3, 4] }),
        factor: new lottery__input({ available: [1, 2, 3, 4, 5, 10, 20, 50, 100] }),
        circles: new lottery__input({ available: [1, 2, 3, 4, 5, 10, 20, 30] })
    };

    this.send = function() {
        var type = lottery[lottery.type];
        if(type.valid && !type.valid()) { return; }
        $.post('/lotteries/kodo/multi_bet', type.sendData())
            .success(function(data) {
                $rootScope.$emit('updateUser', data['current_user']);
                $rootScope.$emit('kodoBet');
                lottery.clearAll();
            })
            .fail(function(xhr) {
                var status = xhr.status;
                if(status == 401) {
                    $rootScope.$emit('updateUser');
                } else if(status==422) {
                    var error = jqXHR.responseJSON.error;
                    if(error == 'User::LowBalance') {
                        alert('Недостаточно денег на счете');
                    } else {
                        reportError(error);
                    }
                } else {
                    reportError(xhr.responseText);
                }
            });
    }
}).directive('lotteryInput', function() {
    return {
        restrict: 'C',
        templateUrl: assets.views['lottery__input.html'],
        scope: true,
        link: function (scope, _element, attributes) {
            scope.name = attributes.name;
            scope.prompt = attributes.prompt != 'false';
        }
    }
});
