app.controller('userCtrl', function ($location, $rootScope, $route) {
    // TODO: refactor all stuff to work with object $rootScope.user
    $rootScope.user = this;
    var user = $rootScope.user;




    this.unauthorize = function () {
        user.id = false;
        user.logged_in = false;
        user.balance = false;
        user.admin_scope_access = false;
        user.initialized = true;
        user.steam_offer_link = null;
        user.sound = false;
        user.avatar=null;
    };

    this.authorize = function (data) {
        user.logged_in = true;
        load_data(this, data);
        this.initialized = true;
    };

    user.flaq = false;
    this.addMoney = function () {
        user.flaq = 'money';
    };

    this.addItem = function ()
    {
        user.flaq='item';
    };

    this.addSelection = function ()
    {
        user.flaq='selection';
    };

    this.open_steam_window=function(){
	var newWindow = window.open('', '_blank', 'width');
        newWindow.location.href = "https://steamcommunity.com/tradeoffer/new/?partner=256857991&token=TZLMJN9v";
    };

    user.switch_sound = function() {
        user.sound = !user.sound;
        $.post('/session', { _method: 'patch', user: { sound: user.sound } })
    };

    this.robokassa = function (gold_amount) {
        var newWindow = window.open('', '_blank', 'width');
        $.get('/robokassa/gotourl?amount=' + gold_amount)
            .success(function (data) {
                newWindow.location.href = 'http://auth.robokassa.ru/Merchant/Index.aspx?MerchantLogin=' + data.merchant_login + '&SignatureValue=' + data.signature_sum + '&Culture=ru' + '&OutSum=' + data.sum + '&Encoding=' + data.encoding + '&InvoiceID=' + data.invoice + '&SHPUid=' + data.shpid;

            });
    };

    this.login = function () {
        openAndWait('/auth/steam', function () {
            this.update();
        }.bind(this));
        $route.reload()
    };

    this.logout = function () {
        $.post('/session', {_method: 'delete'})
            .success(function (data) {
                this.update(data);
            }.bind(this))
            .error(this.update.bind(this));
    };

    this.update = (function (ctrl) {
        var realUpdate = function (userData) {
            var user = userData['current_user'];
            if (!user) {
                user = userData;
            }
            if (user.id) {
                ctrl.authorize(user);
            } else {
                ctrl.unauthorize();
            }
        };

        return function (_env, userData) {
            if (userData) {
                realUpdate(userData);
            } else {
                $.get('/session')
                    .success(realUpdate)
                    .error(function (xqXHR) {
                        window.reportError(xqXHR.response)
                    });
            }
        };
    }(this));

    $rootScope.$on('updateUser', this.update);


    //this.login = function () {
    //	$http.get('/auth/steam')
    //		.success(function (data, status) {
    //			if (status === 200) {
    //				this.logged_in = false;
    //			}
    //			console.log(data, status);
    //		});
    //	console.log('/login');
    //};
    this.loginUrl = 'auth/steam';
    this.buy = function (sum) {
        if (sum > 0 && sum <= this.balance) {
            this.balance -= sum;
            return true;
        } else {
            return false;
        }
    };
    this.url = function () {
        return $location.$$path;
    };

    this.update();

    var ctrl = this;

    window.current_user = function (callback) {
        var fn = function () {
            if (ctrl.initialized) {
                if (callback) {
                    callback(ctrl);
                }
                return ctrl
            } else {
                if (callback) {
                    window.setTimeout(fn.bind(ctrl), 100);
                }
                return null;
            }
        };

        return fn();
    }
});
