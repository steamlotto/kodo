SuperPrize = 0;
function GetStats(answer) {
    if (SuperPrize != 1) {
        SuperPrize = 1;
        $.ajax({
            type: "GET",
            url: "/stats",
            success: function (data) {
                $(answer).html(data.superprize_current+" золотых")
            },
            dataType: 'json'
        });
    }
    else {
        SuperPrize = 0;
        $(answer).html("Суперприз");
    }
}