app.controller('archiveCtrl', function ($http) {
    var archive = this;
    this.toggleSuperprize = function () {
        if (this.filter.superprize) {
            this.filter.superprize = undefined;
        } else {
            this.filter.superprize = true;
        }
    };
    this.order = false;
    archive.lotteries = [];

    //filter
    this.filter = {};
    this.filter.name = 'circle';
    this.filter.circle = undefined;
    this.filter.superprize = undefined;
    this.filter.getCirlce = function () {
        if (this.filter.circle = '') {
            return undefined
        } else {
            return this.filter.circle;
        }
    };
    //guns
    this.getTheme = function (index) {
        return guns.fetch(index).theme;
    };
    this.getPreview = function (index) {
        return guns.fetch(index).preview;
    };
    this.getTitle = function (index) {
        return guns.fetch(index).title;
    };
    this.getSubtitle = function (index) {
        return guns.fetch(index).subtitle;
    };
    //paging
    archive.paginationCurrentLink = 1;
    lastPaginationLink = 100;
    //TODO: fix it ^
    archive.paginationLinks = [];
    this.paginationClick = function (index) {
        if (index != "...") {
            $.get('/lotteries/kodo/last?count=10&offset=' + index + '0')
                .success(function (data) {
                    archive.lotteries = data.lotteries;
                    paginationCurrentLink = index;
                    archive.paginationLinks = archive.generatePagingLinks();
                })
        }
    };

    this.getChosen = function(index) {
        if(index == paginationCurrentLink) {
            return 'current__pagination'
        } else {
            return ''
        }
    };

    archive.generatePagingLinks = function () {
        if (paginationCurrentLink <= 4) {
            return [lastPaginationLink, '...', 6, 5, 4, 3, 2, 1]
        } else if (paginationCurrentLink >= lastPaginationLink - 4) {
            return [lastPaginationLink, lastPaginationLink - 1, lastPaginationLink - 2, lastPaginationLink - 3, lastPaginationLink - 4,
                lastPaginationLink - 5, '...', 1]
        } else {
            return [lastPaginationLink, ' ...', paginationCurrentLink + 2, paginationCurrentLink + 1, paginationCurrentLink,
                paginationCurrentLink - 1, paginationCurrentLink - 2, '...', 1]
        }

    };
    //date
    this.filter.date = {};
    this.filter.date.min = new Date(2015, 1, 12);
    this.filter.date.max = new Date();
    //time
    this.filter.time = {};
    this.filter.time.min = new Date(2015, 1, 12);
    this.filter.time.max = new Date();

    this.paginationClick(1);
});
// app.filter('dateFilter', function () {
// 	return function (input, date) {
// 		var min = date.min.getTime(),
// 			max = date.max.getTime(),
// 			show = input;
// 		angular.forEach(input, function (value,key) {
// 			// console.log();
// 			if(value.date > min, value.date < max){

// 			}else{
// 				show = false;
// 			}
// 		});
// 		return show;
// 	}
// });