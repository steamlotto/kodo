app.controller('broadcastCtrl', function ($rootScope, $interval, $timeout) {
    var broadcast = this;
    this.auto = true;
    this.shown = false;

    this._update = function (data) {
        var lottery = data['lottery'],
            result = lottery.result;
        broadcast.id = lottery.id;
        broadcast.gunIcon = guns[result.gun - 1].preview;
        broadcast.numbers = result.numbers;
    };

    this.show = function (_event, number, force) {
        var interval = 0,
            _show = function () {
                $.get('lotteries/kodo/wait', {number: number}).done(function (data) {
                    $rootScope.$emit('lotteryFinished', data);
                    if (broadcast.auto || force) {
                        broadcast.shown = true;
                    }
                    broadcast._update(data);
                    $interval.cancel(interval);
                }).error(function (jqXHR) {
                    if (jqXHR.status == 404) {
                        // It's ok
                    } else {
                        $interval.cancel(interval);
                        reportError(jqXHR.responseText);
                    }
                });

            };
        if (force) {
            _show();
        }
        else {
            interval = $interval(_show, 1000, 10);
        }
        $timeout(function(){broadcast.shown = false;}, 45000);
    };



    $rootScope.$on('showBroadcast', this.show);
})
    .directive('broadcast', function () {
        return {
            templateUrl: assets.views['broadcast.html']
        };
    });
