app.controller('callbackCtrl', function ($http) {
	var callback = this;

	this.ticket = {};
	this.errors = {};
	this.ticket.category = 'else';
	this.sended = false;
	this.addTicket = function () {
		var request = this.ticket;
		$.post('tickets', { ticket: request })
			.success(function(x){
                callback.ticket = {};
                callback.errors = {};
                callback.sent = true;
            })
			.fail(function (x) {
				if(x.status === 422) {
                    callback.errors = x.responseJSON.errors.ticket;
                } else {
                    reportError(x.responseText)
                }
			});
	}
});
