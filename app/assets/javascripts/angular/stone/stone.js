var Stone = {registry: [], timer: [], time: [], users: [],rounds:[]};
Stone.registry['game']={};
Stone.registry['rounds']=[];
Stone.registry['status'] = 'close';
app.controller('stoneCtrl', function ($rootScope, $timeout) {
    var stone = this;

    stone.timer = Stone.timer;
    stone.time = Stone.time;
    Stone.rootScope = $rootScope;
    stone.registry = Stone.registry;
    stone.lobby = Stone.lobby;
    stone.round = Stone.round;

    stone.users = Stone.users;
    stone.search=Stone.search;

    stone.getGame = function () {
        stone.lobby.setDefaultsFactorButtons(Stone.rootScope);
        $.get('/stones?object=current')
            .success(function (data) {
		if (data!=null)
		{
                if ((data.game=='empty')||(data.game==null))
                {
                    stone.registry['status']='close';
                }
                else
                if (data.game!='empty')
                {
                    stone.registry['game']=data.game;
                    if (data.game.status!='close')
                    {
                        stone.registry['status']='search';
                        stone.search.start();
                    }
                    else
                    {
                        stone.registry['status']='close';
                        stone.registry["game"].factor=0;
                    }
                }
		}
            })
            .fail(function () {
                alert("Ошибка соединения с сервером");
            });
    };



    stone.sendBet = function (bet) {
        console.log('send bet');
        stone.registry['lastbet'] = bet;
        $.ajax({
            url: '/stones/bet',
            type: "POST",
            data: {
                bet: bet,
                game_id: Stone.registry['game'].id
            }
        })
            .success(function (data) {
                console.log(data)
            })
    };


    stone.sendFactor = function () {
        $.ajax({url: '/stones/participate', type: 'POST', data: {factor: stone.registry['game'].factor}})
            .success(function () {
                stone.getGame();
            })
    };


    stone.sendCloseGame = function () {
        console.log('close game');
        $.ajax({
            url: '/stones/finish',
            type: 'POST',
            success: function (data) {
                stone.registry['status'] = 'close';
            }
        });
    };

    stone.getGame();

});