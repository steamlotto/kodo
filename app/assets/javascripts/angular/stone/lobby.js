Stone.lobby={};


    Stone.lobby.setDefaultsFactorButtons = function (balance) {
        console.log('set defaults lobby');
        Stone.lobby.factor=0;
        if (balance < 1000) {
            Stone.lobby.fourthButtonStatus = 'grey';
        } else {
            Stone.lobby.fourthButtonStatus = 'blue';
        }
        if (balance < 500) {
            Stone.lobby.thirdButtonStatus = 'grey';
        } else {
            Stone.lobby.thirdButtonStatus = 'blue';
        }
        if (balance < 100) {
            Stone.lobby.secondButtonStatus = 'grey';
        } else {
            Stone.lobby.secondButtonStatus = 'blue';
        }
        if (balance < 10) {
            Stone.lobby.firstButtonStatus = 'grey';
        } else {
            Stone.lobby.firstButtonStatus = 'blue';
        }
    };

    Stone.lobby.setFactor=function(index){
        var balance=Stone.rootScope.user.balance;
        Stone.lobby.setDefaultsFactorButtons();
        if (index == 'first' && balance >= 10) {
            Stone.lobby.firstButtonStatus = 'yellow';
            Stone.registry['game'].factor = 10;
        }
        if (index == 'second' && balance >= 100) {
            Stone.lobby.secondButtonStatus = 'yellow';
            Stone.registry['game'].factor = 100;
        }
        if (index == 'third' && balance >= 100) {
            Stone.lobby.secondButtonStatus = 'yellow';
            Stone.registry['game'].factor = 100;
        }
        if (index == 'fourth' && balance >= 100) {
            Stone.lobby.secondButtonStatus = 'yellow';
            Stone.registry['game'].factor = 100;
        }
    };