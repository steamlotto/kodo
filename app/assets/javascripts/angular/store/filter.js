Store.Filter = function(params) {
    load_data(this, params);
    if(params.attribute) {
        this.matcher = function(item, selection) {
            return item[params.attribute] == selection
        };
    } else {
        this.matcher = params.matcher;
    }
    this.clear();
};

Store.Filter.prototype = {
    match: function(item) {
        if(this.selection === null) { return true; }
        return this.matcher(item, this.selection)
    },
    clear: function() {
        this.selection = this.default
    },
    select: function(value) {
        this.selection = value;
    }
};

Store.pluckFilters = function(data) {
    var tap = function() {
        if(this.selection) {
            this.select(null);
        } else {
            this.select(true);
        }
    };

    var result = {
        type: new Store.Filter({
            title: 'Тип',
            values: data['weapon_types'],
            attribute: 'weapon_type',
            default: null
        }),
        rareness: new Store.Filter({
            title: 'Качество',
            values: data['rarenesses'],
            attribute: 'rareness',
            default: null
        }),
        best: new Store.Filter({
            matcher: function (item) {
                return !item['best']
            }
        }),
        stared: new Store.Filter({
            attribute: 'stared',
            tap: tap,
            default: null
        }),
        stattrak: new Store.Filter({
            attribute: 'stattrak',
            tap: tap,
            default: null
        }),
        query: new Store.Filter({
            matcher: function(item) {
                if(this.selection == '') { return true }
                return ~item.name.toLowerCase().indexOf(this.selection.toLowerCase());
            },
            default: ''
        })
    };

    result.clear = function() {
        $.each(this, function() {
            if(this.clear) { this.clear(); }
        })
    };

    return result;
};

app.filter('titledFilters', function() {
    return function(hash) {
        var result = [];
        $.each(hash, function() {
            if(this.title != undefined) { result.push(this) }
        });
        return result;
    }
});

