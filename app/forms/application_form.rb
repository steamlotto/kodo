class ApplicationForm < Draper::Decorator
  AccessError = Class.new(StandardError)

  include ActiveModel::Validations

  delegate :persisted?, :model, :to_key
  delegate :column_for_attribute, :has_attribute?
  delegate :save!

  validate do
    notifications.each do |key, values|
      values.each do |value|
        errors.add(key, value)
      end
    end

    object.valid?

    object.errors.each do |key, value|
      errors.add(key, value)
    end
  end

  class <<self
    delegate :lookup_ancestors, to: :object_class

    def permit(*keys, read_access: nil, write_access: nil)
      setters = keys.map { |x| :"#{x}=" }

      with_access(keys, read_access) unless read_access == false
      with_access(setters, write_access) unless write_access == false
    end

    private

    def with_access(keys, access)
      keys.each do |key|
        define_method(key) do |*args|
          raise AccessError if access && !h.can?(access)
          object.send(key, *args)
        end
      end
    end
  end

  def save
    save! if valid?
  end

  def initialize(x = nil, *args)
    if x
      super(x, *args)
    else
      super(self.class.object_class.new, *args)
    end
  end

  def update_attributes(params)
    @notifications = nil # Just reset
    check_access!
    params.each do |key, value|
      begin
        send(:"#{key}=", value)
      rescue AccessError => e
        notifications[key] << e.message
      end
    end
  end

  def submit(params)
    @old_attributes = object.attributes.dup
    update_attributes params
    object.tap(&:save!) if valid?.tap { |valid| report!(valid) }
  end

  def notifications
    @notifications ||= Hash.new { |hash, key| hash[key] = [] }
  end

  protected

  def report!(_valid); end

  def check_access!; end
end
