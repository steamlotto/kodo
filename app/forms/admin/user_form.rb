class Admin::UserForm < ApplicationForm
  decorates :user
  delegate_all

  AVAILABLE_ROLES = {
    user: 'Юзер',
    manager: 'Менеджер',
    admin: 'Админ',
    disabled: 'Отключен',
  }.stringify_keys

  validates :role, inclusion: { in: AVAILABLE_ROLES.keys, message: 'Неизвестная роль' }

  def created_at
    object.created_at.to_date
  end

  def roles_collection
    {
      collection: AVAILABLE_ROLES.invert,
      disabled: AVAILABLE_ROLES.keys.reject { |x| h.can? x },
      include_blank: false,
    }
  end

  def role
    user[:role]
  end

  def role=(new_role)
    raise AccessError unless h.can?(new_role)
    user.role = new_role
  end

  protected

  def check_access!
    raise AccessError unless h.can?(role)
  end
end
