class InventoryReloadProcessor < Que::Job
  def run(params)
    item_classes, items = %w[item_classes items].map { |x| params.fetch(x) }

    item_classes = item_classes.map do |steam_id, attrs|
      [steam_id, ItemClass.find_or_create_by(steam_id: steam_id) { |x| x.update_attributes(attrs) }]
    end.to_h

    items.each do |steam_id, item_class_steam_id|
      Item.find_or_create_by(steam_id: steam_id) do |x|
        x.item_class = item_classes[item_class_steam_id]
      end
    end

    Item.where.not(steam_id: items.keys).where(bot_order: nil).delete_all
  end
end

__END__
