class AlterUserRenameSteamOffetIdSteamOfferLink < ActiveRecord::Migration
  def change
    rename_column :users, :steam_offer_id, :steam_offer_link
  end
end
