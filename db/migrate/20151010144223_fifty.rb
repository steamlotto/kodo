class Fifty < ActiveRecord::Migration
  def change
    create_table :fifties do |t|
      t.references :user, null: false, index: true
      t.integer :current_step
      t.integer :factor, null: false
      t.boolean :game_done
      t.integer :win_amount, array: true, default: '{0, 0, 0, 0, 0, 0, 0, 0, 0, 0}'
      t.integer :history, array: true, default: '{0, 0, 0, 0, 0, 0, 0, 0, 0}'
    end


  end
end
