class CreateBotOrders < ActiveRecord::Migration
  def change
    create_table :bot_orders do |t|
      t.string :status, default: :pending, null: false
      t.references :user, null: false
      t.timestamps null: false
    end

    add_reference :items, :bot_order, index: true, null: true
  end
end
