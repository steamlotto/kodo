class AlterTicketsAddColumns < ActiveRecord::Migration
  def change
    add_column :tickets, :user_name, :string
    add_column :tickets, :user_email, :string
    reversible do |d|
      d.up do
        Ticket.find_each do |ticket|
          name = ticket.user.nickname
          ticket.user_name = name
          ticket.user_email = 'unknown_mail@example.com'
          ticket.save!
        end
      end
    end
  end
end
