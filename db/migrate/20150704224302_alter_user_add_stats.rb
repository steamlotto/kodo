class AlterUserAddStats < ActiveRecord::Migration
  def change
    add_column :users, :payments, :decimal, scale: 2, precision: 10, null: false, default: 0
    add_column :users, :kodo_tickets, :integer, null: false, default: 0
    add_column :users, :headshot_tickets, :integer, null: false, default: 0

    add_column :users, :created_at, :timestamp, null: false, default: Time.zone.now
    add_column :users, :updated_at, :timestamp, null: false, default: Time.zone.now

    reversible do |dir|
      dir.up do
        change_column :users, :created_at, :timestamp, null: false
        change_column :users, :updated_at, :timestamp, null: false
      end
    end
  end
end
