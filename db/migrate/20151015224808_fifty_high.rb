class FiftyHigh < ActiveRecord::Migration
  def change
    create_table :fifty_high do |t|
      t.integer :high
      t.timestamp :end_time
    end
  end
end
