class CreateHeadshotLotteryStats < ActiveRecord::Migration
  def change
    create_table :headshot_lottery_stats do |t|
      t.decimal :bets, scale: 2, precision: 10, default: 0, null: false
      t.decimal :wins, scale: 2, precision: 10, default: 0, null: false
      t.integer :participations_count, default: 0, null: false
      t.integer :winners, default: 0, null: false
      t.hstore :groups, default: {}, null: false

      t.date :date
    end
  end
end
