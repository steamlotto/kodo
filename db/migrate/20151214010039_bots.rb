class Bots < ActiveRecord::Migration
  def change
    create_table :bots do |t|
      t.string :login_steam, null: false, unique: true
      t.string :pass_steam, null: false
      t.string :login_mail, null: false, unique: true
      t.string :pass_mail, null: false
      t.string :role
      t.string :code_link
      t.string :status
      t.string :error
    end
  end
end
