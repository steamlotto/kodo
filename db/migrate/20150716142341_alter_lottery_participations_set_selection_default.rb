class AlterLotteryParticipationsSetSelectionDefault < ActiveRecord::Migration
  def change
    change_column :lottery_participations, :selection, :json, default: {}
  end
end
