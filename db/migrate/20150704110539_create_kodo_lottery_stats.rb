class CreateKodoLotteryStats < ActiveRecord::Migration
  def change
    create_table :kodo_lottery_stats do |t|
      t.integer :participations_count, default: 0, null: false
      t.integer :winners, default: 0, null: false
      t.references :lottery, index: true
      t.decimal :bets, scale: 2, precision: 10, default: 0, null: false
      t.decimal :wins, scale: 2, precision: 10, default: 0, null: false
      t.date :date, null: false
    end
  end
end
