class CreateCreditings < ActiveRecord::Migration
  def change
    create_table :creditings do |t|
      t.decimal :amount, scale: 2, precision: 10, null: :false
      t.references :user, index: true
      t.timestamps null: false
    end
  end
end
