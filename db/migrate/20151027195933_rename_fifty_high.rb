class RenameFiftyHigh < ActiveRecord::Migration
  def up
    rename_table  :fifty_high, :fifty_highs

  end
  def down
    rename_table  :fifty_highs, :fifty_high
  end
end
