class AlterLotteryParticipations < ActiveRecord::Migration
  def change
    enable_extension 'hstore' unless Rails.env.production?
    add_column :lottery_participations, :win_metadata, :hstore, default: {}, null: false
  end
end
