class FirstLastAgg < ActiveRecord::Migration
  def up
    connection.execute <<-SQL
      -- Create a function that always returns the first non-NULL item
      CREATE FUNCTION public.first_agg ( anyelement, anyelement )
      RETURNS anyelement LANGUAGE SQL IMMUTABLE STRICT AS $$
              SELECT $1;
      $$;

      -- And then wrap an aggregate around it
      CREATE AGGREGATE public.FIRST (
              sfunc    = public.first_agg,
              basetype = anyelement,
              stype    = anyelement
      );

      -- Create a function that always returns the last non-NULL item
      CREATE FUNCTION public.last_agg ( anyelement, anyelement )
      RETURNS anyelement LANGUAGE SQL IMMUTABLE STRICT AS $$
              SELECT $2;
      $$;

      -- And then wrap an aggregate around it
      CREATE AGGREGATE public.LAST (
              sfunc    = public.last_agg,
              basetype = anyelement,
              stype    = anyelement
      );
    SQL
  end

  def down
    connection.execute <<-SQL
      DROP AGGREGATE public.FIRST
      DROP AGGREGATE public.LAST
      DROP FUNCTION public.first_agg;
      DROP FUNCTION public.last_agg;
    SQL
  end
end
