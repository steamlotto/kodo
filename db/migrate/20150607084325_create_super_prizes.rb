class CreateSuperPrizes < ActiveRecord::Migration
  def change
    create_table :super_prizes do |t|
      t.string :lottery_type
      t.decimal :amount, precision: 10, scale: 2, default: 0
      t.decimal :ratio, precision: 10, scale: 5, null: false

      t.timestamps null: false
    end

    add_index :super_prizes, :lottery_type, unique: true
  end
end
