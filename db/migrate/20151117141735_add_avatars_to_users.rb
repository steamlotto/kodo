class AddAvatarsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :avatar, :string, null: false, default: 'http://api.adorable.io/avatars/95/abott@adorable.png'
  end
end
