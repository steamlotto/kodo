class CreateItemClassesAndItems < ActiveRecord::Migration
  def change
    create_table :item_classes do |t|
      t.string :name, null: false
      t.string :market_name
      t.string :market_hash_name

      t.string :quantity, null: false
      t.integer :steam_id, null: false

      t.string :icon
      t.string :icon_large
      t.decimal :price, scale: 2, precision: 10

      t.boolean :enabled, null: false, default: false
      t.boolean :best, null: false, default: false

      t.integer :app_id # No index because no usage

      t.index %i[name quantity]
      t.index :steam_id, unique: true
      t.index :best
    end

    create_table :items do |t|
      t.references :item_class, index: true
      t.integer :steam_id, null: false
      t.index :steam_id, unique: true
    end
  end
end
