FactoryGirl.define do  factory :bot do
    
  end
  factory :stone_round do
    
  end
  factory :stone_game do
    
  end

  sequence(:nickname) { |i| "nickname#{i}" }
  sequence(:uid, &:to_i)

  factory :user do
    nickname
    steam_id { generate :uid }

    trait :admin do
      role :admin
    end
  end

  factory :super_prize do
    lottery_type 'Lottery::Base'
    ratio 1.0

    trait :kodo do
      lottery_type 'Lottery::Kodo'
    end
  end

  factory :lottery, class: Lottery::Base do
    time_from 1.day.ago
    time_till 1.day.since
    type 'Lottery::Base'

    factory :kodo_lottery, class: Lottery::Kodo do
      type 'Lottery::Kodo'
    end

    factory :headshot_lottery, class: Lottery::Headshot do
      type 'Lottery::Headshot'

      trait :empty do
        after(:create) { |x| x.participations.delete_all }
      end
    end
  end

  factory :lottery_participation, class: Lottery::Participation do
    user
    lottery
    bet 10
  end

  factory :crediting do
    user
    amount 100
  end

  factory :item_class do
    sequence(:name) { |i| "item class #{i}" }
    steam_id { generate(:uid) }
    weapon_type 'Винтовка'
    rareness 'Армейское производство'
    classfield 'Ширпотреб'
  end

  factory :item do
    item_class
    steam_id { generate(:uid) }
  end

  factory :lottery_lifetime_stat, class: Lottery::LifetimeStat do
    lottery_type 'Lottery::Base'
    data { {} }

    trait :kodo do
      lottery_type 'Lottery::Kodo'
      data do
        {
          numbers: (1..30).map { |x| [x, 0] }.to_h,
        }
      end
    end
  end
end
