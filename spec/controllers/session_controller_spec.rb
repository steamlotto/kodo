describe SessionsController do
  describe 'POST create' do
    let(:example_user) { build :user }
    let(:nickname) { example_user.nickname }
    before do
      info = double nickname: nickname
      auth = double uid: example_user.steam_id, info: info
      request.env['omniauth.auth'] = auth
    end

    subject { -> { post :create } }

    context 'when user existed' do
      let!(:user) { example_user.tap(&:save!) }

      it 'does not create new user' do
        is_expected.not_to change(User, :count)
      end

      context 'when nickname changed' do
        let(:nickname) { generate(:nickname) }

        it 'changes user nickname' do
          is_expected.to change { user.reload.nickname }.to(nickname)
        end
      end
    end

    context 'when user not exists' do
      it { is_expected.to change(User, :count).by(1) }
      # creating another user
      before { create :user }

      it 'stores user steam id' do
        subject.call
        expect(User.find_by(steam_id: example_user.steam_id)).to be_truthy
      end
    end
  end

  describe 'DELETE destroy' do
    let(:user) { create :user }
    before { session[:user_id] = user.id }
    before { delete :destroy, locale: :en }
    subject { response }

    it { is_expected.to redirect_to('/') }

    it 'clears user_id cookie' do
      expect(session[:user_id]).to be_nil
    end

    it 'makes current_user guest' do
      expect(controller.send(:current_user)).to be_guest
    end
  end

  describe 'GET current_user' do
    let(:user) { create :user }
    before { log_in user }
    before { get :get_current_user }
    subject { response }
    it { is_expected.to be_ok }
    include_examples 'includes decorated user'
  end
end
