describe LotteryParticipationsController do
  shared_examples 'participation is decorated' do
    before { participation }
    before { subject }

    let(:participation_result) { response_json[:participations].first.symbolize_keys }

    context 'kodo lottery' do
      let(:lottery) do
        create :kodo_lottery, result: { gun: 2, numbers: (7..14).to_a }
      end

      let(:participation) do
        create :lottery_participation,
               lottery: lottery,
               user: user,
               bet: 150,
               bet_metadata: { circles: [1, 2, 3] },
               selection: {
                 guns: [1],
                 numbers: (1..8).to_a,
               }
      end

      it 'decorates result' do
        expect(participation_result)
          .to match(
            id: participation.id,
            user_id: user.id,
            lottery_id: participation.lottery.id,
            factor: 5,
            win: false,
            guns: [1],
            numbers: be_kind_of(Array),
            circles: match_array([1, 2, 3]),
            created_at: participation.created_at.in_time_zone('Moscow').as_json,
          )
      end

      it 'decorates numbers' do
        numbers = participation_result[:numbers].map { |x| [x['index'], x['status']] }.to_h
        (1..6).each { |x| expect(numbers).to include x => 'selected' }
        (7..8).each { |x| expect(numbers).to include x => 'matched' }
        (9..14).each { |x| expect(numbers).to include x => 'winning' }
        (15..30).each { |x| expect(numbers).to include x => 'empty' }
      end

      context 'participations is winning' do
        let(:participation) do
          create :lottery_participation, user: user, win: 100, lottery: lottery
        end

        it 'sets win to true' do
          expect(participation_result[:win]).to be_truthy
        end
      end
    end

    context 'headshot lottery' do
      let(:lottery) { create :headshot_lottery, :empty }
      let!(:participation) do
        create :lottery_participation, lottery: lottery, user: user,
                                       bet: 20, win: 30, created_at: '1.1.2001'
      end

      it 'decorates result' do
        expect(participation_result).to match(
          id: participation.id,
          user_id: user.id,
          lottery_id: lottery.id,
          updated_at: participation.updated_at.in_time_zone('Moscow').as_json,
          win: '30.0',
        )
      end
    end
  end

  describe 'GET index' do
    def subject
      get :index, id: 'kodo'
    end

    specify 'user is guest' do
      expect { subject }.to raise_error(UserSession::Unauthorized)
    end

    context 'user is logged in' do
      let(:user) { create :user, role: :user, balance: 1_000_000 }
      before { log_in(user) }

      it_behaves_like 'includes decorated user' do
        before { subject }
      end

      context 'there is multiply participations' do
        let(:lottery1) { create :kodo_lottery }
        let(:lottery2) { create :kodo_lottery }
        let!(:participations) do
          [
            2.times.map { create :lottery_participation, lottery: lottery1, user: user },
            3.times.map { create :lottery_participation, lottery: lottery2, user: user },
          ].flatten
        end

        before { subject }

        it 'returns them all as json' do
          expect(response).to be_ok
          expect(response_json[:participations].map { |x| x[:id] })
            .to match_array(participations.map(&:id))
        end
      end

      it_behaves_like 'participation is decorated' do
        def subject
          get :index, id: lottery.to_param
        end
      end
    end
  end

  describe 'GET stats' do
    before { create :lottery_lifetime_stat, :kodo }
    before { create_list(:kodo_lottery, 5).each(&:process_end!) }
    after do
      expect(response).to be_ok
      expect(response_json[:numbers]).to match([be_nil, *(1..30).map { |_x| be_a(Numeric) }])
    end

    specify 'count=all' do
      get :stats, count: :all, id: 'kodo'
    end

    specify 'count=2' do
      get :stats, count: 2, id: 'kodo'
    end
  end

  describe 'GET today' do
    def subject
      get :today, id: 'kodo'
    end

    let(:user) { create :user, role: :user, balance: 1_000_000 }
    before { log_in(user) }

    it_behaves_like 'includes decorated user' do
      before { subject }
    end

    it_behaves_like 'participation is decorated' do
      def subject
        get :index, id: lottery.to_param
      end
    end

    describe 'scoping' do
      before { Timecop.freeze '1.1.2001'.to_date }
      let(:lottery) { create :kodo_lottery }

      let!(:good_participation) do
        create :lottery_participation, lottery: lottery,
                                       user: user,
                                       updated_at: Time.zone.now
      end

      let!(:bad_participation) do
        create :lottery_participation, lottery: lottery,
                                       user: user,
                                       updated_at: 1.day.ago
      end

      it 'renders only today participation' do
        subject
        expect(response_json[:participations].count).to eq 1
        expect(response_json[:participations].first[:id]).to eq good_participation.id
      end
    end
  end
end
