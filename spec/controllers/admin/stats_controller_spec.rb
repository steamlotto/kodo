require 'rails_helper'

RSpec.describe Admin::StatsController, type: :controller do

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #graphics" do
    it "returns http success" do
      get :graphics
      expect(response).to have_http_status(:success)
    end
  end

end
