Capybara.default_driver = :selenium

shared_context 'feature specs helper', type: :feature do
  def log_in(login, password = 'password')
    visit '/en/news'
    fill_in 'Login', with: login
    fill_in 'Password', with: password
    click_button 'Log in!'
  end
end

shared_context 'with user', with_user: true do
  let(:user) { create :user, password: 'password' }
  before { log_in(user.login, 'password') }
end

shared_context 'with admin', with_admin: true do
  let(:user) { create :user, password: 'password', role: :admin }
  before { log_in(user.login, 'password') }
end
