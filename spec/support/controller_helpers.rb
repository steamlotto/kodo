module RSpec::Core::MemoizedHelpers
  def log_in(user)
    controller.send :log_in!, user
  end

  def set_controller(x)
    @controller = x.new
  end

  def response_json
    HashWithIndifferentAccess.new JSON.parse(response.body)
  end
end

shared_examples 'it renders User::LowBalance error' do
  it 'renders User::LowBalance error' do
    expect(response.status).to eq 422
    expect(response_json[:error]).to eq 'User::LowBalance'
  end
end
