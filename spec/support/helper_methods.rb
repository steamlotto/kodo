def subject_lambda(&block)
  subject { block }
end
