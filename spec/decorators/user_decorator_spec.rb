describe UserDecorator do
  let(:user) { raw_user.decorate }
  let(:raw_user) { create :user }

  describe '#guest?' do
    subject { user.guest? }

    context 'user persisted' do
      it { is_expected.to be_falsey }
    end

    context 'new user' do
      let(:raw_user) { build :user }
      it { is_expected.to be_truthy }
    end
  end

  specify '#name' do
    expect(user.name).to eq(user.nickname)
  end
end
