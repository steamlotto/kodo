describe Lottery::Headshot do
  describe 'participation generation' do
    let(:lottery) do
      build(:headshot_lottery).tap do |x|
        allow(x).to receive(:participations_templates).and_return(templates)
        x.save!
      end
    end

    let(:templates) do
      [
        {
          count: 10,
          win: 0,
        },
        {
          count: 8,
          win: 20,
        },
        {
          count: 1,
          win: 40,
        },
      ]
    end

    subject { lottery.participations }

    it 'generates exact passed participations' do
      expect(subject.count).to eq 19
      expect(subject.where(win: 0).count).to eq 10
      expect(subject.where(win: 20).count).to eq 8
      expect(subject.where(win: 40).count).to eq 1
    end
  end

  describe '#process_end!' do
    before { Timecop.freeze '1.1.2001'.to_date }

    let(:lottery) { create :headshot_lottery, time_from: 1.day.ago, time_till: 1.day.since }
    # We have next lottery to be present anyway
    before { lottery.create_next! }
    before { lottery.process_end! }

    it 'finishes lottery' do
      expect(Lottery::Headshot.unfinished).not_to include eql(lottery)
      expect(lottery.time_till).to eq Time.zone.now
    end

    it 'updates next lottery' do
      expect(Lottery::Headshot.running.time_from).to eq(Time.zone.now)
    end
  end

  describe '#participate!' do
    let(:user) { create :user, balance: 20 }
    let(:lottery) { create :headshot_lottery }

    # All participations would be stubbed during test
    before { lottery.participations.delete_all }

    def subject
      lottery.participate! user, {}
    end

    context 'when there is no unused particiaptions left' do
      before do
        10.times { create :lottery_participation, lottery: lottery }
      end

      it 'creates new lottery and raises error' do
        expect(lottery).to receive(:process_end!)
        expect { subject }.to raise_error Lottery::LotteryAlreadyFinished
      end
    end

    context 'when there is winning participation left' do
      before { create :lottery_participation, lottery: lottery, user: nil, win: 40 }

      it 'awards user' do
        expect { subject }.to change { user.reload.balance }.from(20).to(40)
      end

      context 'when user have not enought money to participate' do
        let(:user) { create :user, balance: 19 }

        it 'raises error' do
          expect { subject }.to raise_error User::LowBalance
        end
      end
    end

    context 'when there is loosing participation left' do
      before { create :lottery_participation, lottery: lottery, user: nil, win: 0 }

      it 'does not award user' do
        expect { subject }.to change { user.reload.balance }.from(20).to(0)
      end
    end
  end
end
