describe Lottery::Participation do
  let(:participation) { create :lottery_participation, bet: 10 }

  specify '#loose!' do
    expect { participation.loose! }
      .to change { participation.reload.win }.from(nil).to(0)
  end

  describe '#win!' do
    def self.expectation(new_win, old_win: nil, **params)
      specify "changes #win from #{old_win} to #{new_win} with #{params}" do
        participation = create :lottery_participation, bet: 10, win: old_win
        participation.win!(**params)
        expect(participation.reload.win).to eq(new_win)
      end
    end

    expectation 10
    expectation 20, amount: 20
    expectation 30, ratio: 3
    expectation 40, amount: 20, ratio: 2
    expectation 50, old_win: 40
    expectation 200, old_win: 100, amount: 50, ratio: 2

    it 'gives money to user' do
      expect { participation.win! amount: 20 }
        .to change { participation.user.reload.balance }.by(20)
    end
  end
end
