require File.expand_path('../boot', __FILE__)
require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Kodo
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    extra_types = "{#{%w[validators forms jobs].join(',')}}"

    config.eager_load_paths += Dir["#{config.root}/app/#{extra_types}/{concerns,}"]
    config.active_record.raise_in_transactional_callbacks = true
    config.sass.load_paths << File.expand_path('../../vendor/assets/stylesheets/')
    config.active_record.schema_format = :sql
    config.assets.paths << Rails.root.join('app', 'assets', 'fonts')

    def method_missing(name, *)
      data.fetch(name.to_s) { super }
    end

    def logs_path
      if Rails.env.production?
        Pathname.new '/var/rails/shared/logs'
      else
        Rails.root.join 'log'
      end
    end

    def respond_to_missing?(name, *)
      data.key?(name.to_s) || super
    end

    private

    def data
      @data ||= begin
        file = (Rails.root.join('config', 'config.yml'))
        YAML.load(ERB.new(File.read(file)).result)[Rails.env]
      end
    end
  end
end
