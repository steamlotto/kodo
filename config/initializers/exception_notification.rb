unless Rails.application.consider_all_requests_local
  Rails.application.config.middleware.use(
    ExceptionNotification::Rack,
    slack: {
      webhook_url: 'https://hooks.slack.com/services/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',
      channel: '#production',
      additional_parameters: {
        icon_url: 'http://troll-face.ru/static/images/fk_that_original.png',
        mrkdwn: true,
      },
    },
  )
end
