module Enumerable
  def group_and_count
    group_by { |x| x }.map { |k, v| [k, v.length] }.to_h
  end
end
