class Module
  def method_stub(*args)
    args.each do |name|
      define_method(name) { raise NotImplementedError }
    end
  end
end
