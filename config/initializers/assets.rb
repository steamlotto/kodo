# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.

Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js.rb, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.paths << 'app/assets/views'
Rails.application.assets.register_engine('.slim', Slim::Template)

Rails.application.config.assets.precompile += %w[admin.js admin.css angular.js angular-route.js]
Rails.application.config.assets.precompile +=
  Dir[Rails.root.join 'app/assets/views/**/*.{htm,html}']

module ViewContext
  attr_accessor :output_buffer

  def output_buffer_with_sprockets=(_buffer)
    unless is_sprockets?
      # output_buffer_without_sprockets=(buffer)
    end
  end

  def is_sprockets?
    try(:environment).class == Sprockets::Index
  end

  def self.included(klass)
    klass.instance_eval do
      alias_method_chain :output_buffer=, :sprockets
    end
  end
end

Rails.application.assets.context_class.class_eval do
  include ViewContext
end
