deploy_to = '/var/rails'
shared = "#{deploy_to}/shared"
log_file   = "#{shared}/logs/unicorn.log"
err_log    = "#{shared}/logs/unicorn_error.log"
pid_file = "#{shared}/pids/unicorn.pid"
old_pid = pid_file + '.oldbin'

timeout 30
worker_processes 4
pid pid_file
stderr_path err_log
stdout_path log_file
preload_app true

before_fork do |server, _worker|
  defined?(ActiveRecord::Base) and
    ActiveRecord::Base.connection.disconnect!

  # Zero downtime deploy magic
  if File.exist?(old_pid) && server.pid != old_pid
    begin
      Process.kill('QUIT', File.read(old_pid).to_i)
    rescue Errno::ENOENT, Errno::ESRCH
      # someone else did our job for us
    end
  end
end

after_fork do |_server, _worker|
  next unless defined?(ActiveRecord::Base)
  ActiveRecord::Base.establish_connection
end
