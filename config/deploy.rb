# config valid only for current version of Capistrano
lock '3.4.0'

set :bundle_flags, '--deployment --verbose' if ENV['DEBUG']

set :application, 'kodo'
set :repo_url, 'git@bitbucket.org:steamlotto/kodo.git'

# Default branch is :master
ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/var/rails'

# https://github.com/capistrano/rvm/#ruby-and-gemset-selection-rvm_ruby_version
set :rvm_ruby_version, '2.2.2@kodo'

# Unicorn configuration
set :unicorn_options, '-p 3000'
set :unicorn_config_path, File.join(current_path, 'config', 'unicorn.rb')
set :unicorn_pid, File.join(shared_path, 'pids', 'unicorn.pid')

set :slack_webhook, 'https://hooks.slack.com/services/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' # comes from inbound webhook integration
set :slack_channel, '#production'
set :slack_icon_url, -> { 'http://s5.pikabu.ru/post_img/big/2015/04/23/8/1429794583_343353562.png' }

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

desc 'restart workers'
task :restart_workers do
  # Fuck this shit!
  # TODO: write
end

set :linked_files, %w{config/database.yml}

after 'deploy:publishing', 'deploy:restart'
namespace :deploy do
  task :restart do
    invoke 'unicorn:restart'
    invoke 'restart_workers'
  end
end
